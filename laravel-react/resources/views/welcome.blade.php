<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Suburb Access</title>
    <!-- Styles -->
    <!-- <link rel="stylesheet" href="//unpkg.com/leaflet@1.6.0/dist/leaflet.css" /> -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" /> -->
    
</head>



<div id="main" class="app-body" data-usr="{{ auth()->user() }}" ></div>   
<script src="{{ asset('js/app.js') }}" ></script>

</html>
