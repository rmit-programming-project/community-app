/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//require('./components/App');
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import store from "./store";
import App from './components/App'
import { connect, Provider } from "react-redux";

const test = () =>{
  return(
    <div>HELLO WORLD</div>
  )
}

class Root extends Component {
 
    constructor(props){
      super(props);
      
    }
    
    render () {

      // const propsContainer = document.getElementById("main");
      // const props = Object.assign({}, propsContainer.dataset);
      // console.log(store)
      // console.log("USER FROM root ", props.usr)
      // //console.log("props = ", this.props)
      return (
        <Provider store={store}>
          <App></App>
        </Provider>
      )
    }
  }
  
  ReactDOM.render(<Root/> , document.getElementById("main"))
  export default Root;




