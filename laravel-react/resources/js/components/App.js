import React, { Component } from 'react'

import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { connect } from "react-redux"
import Header from './Header'
import Home from './pages/home/home'
import LoginPage from './pages/login/login'
import RegisterPage from './pages/register/register'
import {login, setUserData, registerUser, getFavourites} from "./stores/app/actions"

import formData from 'form-data';
import axios from 'axios';





  
  const loggedIn = (user, login, history, register) =>{
    return((user)? 
      <Switch>
        <Route path="/login" component={Home}/>
        <Route path="/register" component={Home}/>
        <Route exact path="/" component={Home}/>
      </Switch> : 
      <Switch>
        <Route path="/login" component={() => <LoginPage login={login} />}/>
        <Route exact path="/" component={() => <LoginPage login={login} />}/>
        <Route path="/register" component={() => <RegisterPage register={register} />}/>
      </Switch>
    )
  }

    class App extends Component {


      constructor(props){
        super(props);
        
        }

      componentDidMount() {
        const propsContainer = document.getElementById("main");
        const props = Object.assign({}, propsContainer.dataset);
        if(!props.usr){
          // console.log("USER IS LOGGED OUT")
        }else{
          // console.log("USER IS LOGGED IN")
          this.props.setUserData(JSON.parse(props.usr))
          this.props.getFavourites(JSON.parse(props.usr).id)
          //this.props.getFavourites(props.usr.id)
        }    
        
      }

      loginUser = async (eml, psw) => {
        this.props.login(eml, psw)
        
        
      }
      
    
      
      render () {
        const propsContainer = document.getElementById("main");
        const props = Object.assign({}, propsContainer.dataset);
        // console.log("USER FROM root ", props.usr)
      //console.log("props = ", this.props)
        const userSignedIn = this.props.isLoggedIn;
        return (
          <BrowserRouter>
            {loggedIn(userSignedIn, this.loginUser, this.props.router, this.props.registerUser)}
          </BrowserRouter>
          
        )
      }
    }
    export const mapDispatchToProps = dispatch =>{
      return {
        login: (email, psw) => {
          dispatch(login(email, psw));
        }, 
        setUserData: (data) => {
          dispatch(setUserData(data))
        },
        registerUser: (name, email, password) => {
          dispatch(registerUser(name, email, password))
        },
        getFavourites: (id) => {
          dispatch(getFavourites(id))
        }

    
      };
    }

    export const mapStateToProps = store => {
      return {
        app: store.app,
        isLoggedIn: store.app.user.isLoggedIn,
        userData: store.app.user.userData
      };
    };
                          // --in--       --out--
    export default connect(mapStateToProps,mapDispatchToProps)(App);


