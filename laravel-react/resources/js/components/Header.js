import React, {Component}  from 'react'
import { Link } from 'react-router-dom'
import { connect } from "react-redux";
import {setName} from "./stores/app/actions"
import {updateMap, setPostcode, setPostcodes} from "./stores/app/actions"
import { KeyboardEvent } from "react";


class Header extends Component {

  constructor(props){
    super(props);

      this.updateInput = this.updateInput.bind(this);
    }
  inputValue = ''

  updateInput(event){
    // console.log(event.target.value)
    this.props.setPostcode(event.target.value) 
  }

  submit = () => {
    let input = this.props.postcode.toString()
    // console.log("button pressed with code", this.props.postcode)
    // this.props.getWeatherNow("TEST")
    // this.props.getWeatherAverage("test")
    this.inputValue = ''
    if(this.props.postcode){
      const postcodeList = this.props.postcodes
      if(!postcodeList.includes(input)){
        postcodeList.push(input)
        // console.log("POST CODE LIST = ", postcodeList)
        // console.log("POST CODE LIST LENGTH = ", postcodeList.length)

        this.props.setPostcodes(postcodeList)
        this.props.updateMap(input)
      }
     
    }
    
        
  }

  handleSubmit(event){
    event.preventDefault();
  }


  render() {
    const {postcode,postcodes} = this.props; 
    
    return(
      <nav className='navbar container'>
        
        <form className="container" onSubmit={this.handleSubmit}>
        <input type="text" onChange={this.updateInput} value={postcode} placeholder="Enter Postcode"></input>
        <button onClick={this.submit} type="submit" disabled={(postcodes.length >= 2 ? true:false)}>Update Map</button>
        </form>

        
      </nav>
    )
  }
}

export const mapDispatchToProps = dispatch =>{
  return {
    setPostcode: async (data) => {
      dispatch(setPostcode(data));
    },
    updateMap: async (data) => {
      dispatch(updateMap(data));
    },
    setPostcodes: async (data) => {
      dispatch(setPostcodes(data))
    },
    download: async (data) => {
      dispatch(download(data))
    }

  };
}

export const mapStateToProps = store => {
  return {
    postcode: store.app.map.postcode,
    postcodes: store.app.map.postcodes,
    app: store.app
  };
};
                      // --in--       --out--
export default connect(mapStateToProps, mapDispatchToProps)(Header);

