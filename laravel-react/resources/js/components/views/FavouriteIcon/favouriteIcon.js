import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { connect } from "react-redux"
import {addFavourite} from '../../stores/app/actions'
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Star from '@material-ui/icons/Star';



class Favourite extends Component {


    constructor(props){
        super(props);
 
    }
  

    toggleFavourite = () =>{
        
        this.props.addFavourite(this.props.generalData[0].id, this.props.user.id, this.props.favourites, this.props.postcode)
        
    }


    render() {
        
        const fav = this.props.favourites.filter((fav)=>fav.postcode==this.props.postcode)
        const isFav =  (fav.length >= 1)? true : false;

        return(
            (this.props.postcode)?
            <div style={{width: 60, height: 60}}>
                <IconButton onClick={this.toggleFavourite} style={{width: '100%', height: '100%'}}>
                    <Star style={{color: isFav? "yellow" : "grey", backgroundColor: "lightgrey", width: '100%', height: '100%'}}></Star>
                </IconButton>
            </div> :
            <div></div>
            
        )
    }
        
    
}

export const mapStateToProps = store =>{
    return {
        app: store.app,
        user: store.app.user.userData,
        favourites: store.app.user.favourites
    };
}

export const mapDispatchToProps = dispatch =>{
    return {
      addFavourite: (suburbId, userId, currentFaves, postcode) => {
        dispatch(addFavourite(suburbId, userId, currentFaves, postcode));
      }
  
    };
  }

export default connect(mapStateToProps, mapDispatchToProps)(Favourite)