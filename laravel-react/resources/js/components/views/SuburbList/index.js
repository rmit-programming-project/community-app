import React from 'react';
import { element } from 'prop-types';
import { connect } from "react-redux"
import Chip from '@material-ui/core/Chip';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import { positions } from '@material-ui/system';
import Backdrop from '@material-ui/core/Backdrop';

const useStyles = makeStyles((theme) => ({
   
   
   box: {
     display: 'flex',
   },
   enteredpostcodes:{
      marginTop:"2%",
   }
 
   
   

 }));

const SuburbList = (props) => {
   const{mapData, postcodes} = props.map;
   const{loaded,invalid} = props;
   // console.log(invalid)
   const handleDelete = (postcode) => () => {
      // console.info('You clicked the delete icon.', postcode);
      props.removeSearch(postcode);
    };

   
   const items = postcodes.map((element,key) => <Chip label={element} key={key} onDelete={handleDelete(element)} />) 
   // console.log("Post codes =", postcodes)

   return(
      
      <div className="enteredpostcodes">
         <Box zIndex="modal" className="box spinner" display={(loaded == true)?"none":"block"}>
            <CircularProgress />
         </Box>
         <Box zIndex="modal" className="box spinner" display={(invalid == true)?"block":"none"}>
            Invalid postcode
         </Box>
         <Box zIndex="modal" className="box postcodes" display={(loaded == true)?"block":"none"}>
            {(postcodes.length > 0)? items : <div>NO POSTCODES ENTERED</div>}
        </Box>

      {/* <Backdrop className={classes.backdrop}>
         <CircularProgress color="inherit" />
      </Backdrop> */}

      </div>
   )
}



export default SuburbList;






// export class SuburbList extends React.Component {

   

//    componentDidMount() {
      
//    }


//    shouldComponentUpdate() {
//       return true;
//    }

//    handleChange = (event) => {
      
//     };

//    AllPostcodes(){
//       const all_postcodes = [];
//       const{response} = this.props;
//       console.log("updating with", response)
//       response.forEach(element => {
//          if(!(all_postcodes.includes(element.postcode))){
//             all_postcodes.push(element.postcode);
//          };
//       });
//       console.log(all_postcodes)
//       return all_postcodes;

//    }

//    render() {
//       const all_postcodes = this.AllPostcodes();
      
//       //TODO ADD ONLICK to REMOVE POSTCODE FROM STORE. 

//       const items = all_postcodes.map((element,key) =>  <div className="postcode-item" id={key}>{element}</div>);
//       return (

//          <div className="container entered-postcodes">
//             {items}
//          </div>
      

//       );
//    }

// }

// export const mapStateToProps = store => {
//    return {
//       postcode: store.map.postcode,
//       response: store.map.mapData
//    }
// }



// export default connect(mapStateToProps, null)(SuburbList)
