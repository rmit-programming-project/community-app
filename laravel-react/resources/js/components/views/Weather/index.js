import React from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import styled from 'styled-components';
import axios from 'axios';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const getRows = (avgs)=>{
   var rows = []
   try{
      avgs.forEach((month) => {
         rows.push(createData(month['name'], month['avg_temp'], month['avg_rain'], month['avg_sun'],month['rain_days']))
      })
   }catch(e){
      rows = []
   }
   return rows;
}

function createData(month, temp, avg_rain, sun_days,rain_days) {
   return { month, temp, avg_rain, sun_days,rain_days};
 }


 function createAverage(avg_temp) {
   return {avg_temp};
}

function getAverages(data){
   // console.log("Start getting averages");
   let dataavailable = 0;
   let avgAvg_temp = 0;
   
   if(data.length > 0){
      data.forEach(month => {
         
         //   if (month[1][0] === undefined) {
         //       return;
         //   }
         // console.log(month['temp']);
         avgAvg_temp += month['temp'];

         dataavailable++;
           
      });

      avgAvg_temp = avgAvg_temp / dataavailable;
      
   }
   return createAverage(avgAvg_temp)
 }


const StyledTableCell = withStyles((theme) => ({
   head: {
     backgroundColor: theme.palette.info.main,
     color: theme.palette.common.white,
   },
   body: {
     fontSize: 14,
   },
 }))(TableCell);

 const StyledTableRow = withStyles((theme) => ({
   root: {
     '&:nth-of-type(odd)': {
       backgroundColor: theme.palette.background.default,
     },
   },
 }))(TableRow);

 
const StyledExpansionSummary = withStyles((theme) => ({
   root: {
       backgroundColor: theme.palette.background.default,
   },
}))(ExpansionPanelSummary);

 const useStyles = makeStyles((theme) => ({
   table: {
      marginTop: '5%',
    //minWidth: 700,
      width: '100%'
  },
  paper: {
     //padding: theme.spacing(2),
     marginLeft: '5%',
     marginRight: '5%',
     marginTop: '2%',
     textAlign: 'center',
     color: theme.palette.text.secondary 
   },
   container: {
      width: '100%',
      textAlign: 'center',
      overflowX: "auto",
   },
   summary: {
      //padding: theme.spacing(2),
      marginLeft: '1%',
      marginBottom: '2%',
      marginTop: '2%',
      textAlign: 'Left',
      paddingTop: '1%',
      color: theme.palette.text.primary,
      
      },
  summaries: {
      width: '100%',
      marginTop: '2%',
      display: 'flex',
      textAlign: 'center', 
      
  },
  summaryItem: {
      width: '100%',
      textAlign: 'center', 
  },
 }));

const Weather = props =>{
   const classes = useStyles();

   
   const cur = props.weatherStats.currentWeather
   const avg = props.weatherStats.averageWeather;
   const air_quality = props.weatherStats.airquality;
   // console.log("MEOW");
   // console.log(avg);
   var rows = getRows(avg)
   var averages = getAverages(rows)
   // console.log(rows)
   // console.log(averages)
  
   
   return (
      <Paper className={classes.paper}>
         <div className={classes.container}>

         <Paper className={classes.summary}  elevation={0}>
                    <h3 width="100%">Weather Stats {(props.postcode === undefined ? ' ': ' | ' + props.postcode)} </h3>
                    <div className={classes.summaries}>
                        <Paper className={classes.summaryItem} elevation={0}>
                             <h4>Air Quality</h4>
                            <Typography className={classes.summaryItem}>{(air_quality[0] === undefined ? air_quality['aqi'] : air_quality['aqi'])}</Typography>
                        </Paper>
                        <Paper className={classes.summaryItem} elevation={0}>
                            <h4>Average Yearly Temp</h4> 
                            <Typography className={classes.summaryItem}>{(averages['avg_temp'] === 0 ? ' ': (averages['avg_temp'].toFixed(1)))}</Typography>
                        </Paper>

                    </div>
               </Paper>
            <ExpansionPanel className={classes.container}>
               <StyledExpansionSummary
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                        className={classes.container}
                    >
                        <div className={classes.container}>
                        {<ExpandMoreIcon />}
                        </div>
                    
               </StyledExpansionSummary>
               <ExpansionPanelDetails className={classes.container}>
                  <div className={classes.container}>
                     <h3>Current Weather</h3>
                     <Table className={classes.table} aria-label="customized table">
                        <TableHead>
                           <TableRow>
                              <StyledTableCell align="center">Temp</StyledTableCell>
                              <StyledTableCell align="center">Clouds</StyledTableCell>
                              <StyledTableCell align="center">Wind Speed</StyledTableCell>
                           </TableRow>
                        </TableHead>
                        <TableBody>
                           <StyledTableRow >
                              <StyledTableCell  align="center">{cur.temp}</StyledTableCell>
                              <StyledTableCell align="center">{cur.clouds}</StyledTableCell>
                              <StyledTableCell align="center">{cur.wind_speed}</StyledTableCell>
                           </StyledTableRow>
                        </TableBody>
                        
                     </Table>
                     <h3>Average(per month)</h3>
                     <Table className={classes.table} aria-label="customized table">
                        <TableHead>
                           <TableRow>
                              <StyledTableCell>Month</StyledTableCell>
                              <StyledTableCell align="center">Average Temp</StyledTableCell>
                              <StyledTableCell align="center">Hours of Sun</StyledTableCell>
                              <StyledTableCell align="center">Rainy Days</StyledTableCell>
                              <StyledTableCell align="center">Average Rain Fall (mm)</StyledTableCell>
                              {/* <StyledTableCell align="right">Air Quality</StyledTableCell> */}
                           </TableRow>
                        </TableHead>
                        <TableBody>
                           {rows.map((row) => (
                              <StyledTableRow key={row.month}>
                                 <StyledTableCell component="th" scope="row"  align="center">
                                    {row.month}
                                 </StyledTableCell>
                                 <StyledTableCell align="center">{row.temp}</StyledTableCell>
                                 <StyledTableCell align="center">{row.sun_days}</StyledTableCell>
                                 <StyledTableCell align="center">{row.rain_days}</StyledTableCell>
                                 <StyledTableCell align="center">{row.avg_rain}</StyledTableCell>
                                 {/* <StyledTableCell align="right">{row.air_quality}</StyledTableCell> */}
                              </StyledTableRow>
                           ))}
                        </TableBody>
                     </Table>
                  </div>
               </ExpansionPanelDetails>
            </ExpansionPanel>
         </div>
      </Paper>
   );
}
export default Weather;

