import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import FavouriteView from '../favourtieView/index'
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import FileSaver from 'file-saver';
import Tooltip from '@material-ui/core/Tooltip';
// import { Tooltip } from 'leaflet';
// import FileSaver from 'file-saver';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const NavBar = (props) => {
    const classes = useStyles();
    const [auth, setAuth] = React.useState(true);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [favouriteStatus, setOpen] = React.useState(false);
    const open = Boolean(anchorEl);
    // console.log("User  data=", props.user)
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleLogout= () => {
        //setAnchorEl(null);
        props.logout();
    };

    const handleDownload = async () => {
        if(props.app.map.postcodes.length > 0){

        let json = JSON.stringify({
            generalData: props.app.general,
            schoolData: props.app.school,
            weatherData: props.app.weather,
            crimeData: props.app.crime,
            transportData: props.app.transport,
        })
        // json = JSON.stringify(json)
        const jsonData = new Blob([json], { json: 'text/json;charset=utf-8;' });

        FileSaver.saveAs(jsonData, 'data.json');
    }

    };

    const handleClickOpen = () => {
        setOpen(true);
      };
    
    const handleCloseFavouriteView = () => {
    setOpen(false);
    };
      

    const handleFavourites = () => {
        setAnchorEl(null);
        setOpen(true);
        //console.log("ADD IMPLEMENTATION FOR FAVOURITES VIEW")
    };

    return (
        <div className={classes.root}>
        <AppBar position="static">
            <Toolbar>
            <Typography variant="h6" className={classes.title}>
                Suburb Access
            </Typography>
            {auth && (
                <div>
                <IconButton
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
                >
                    <AccountCircle />
                    <Typography className={classes.title}>
                        {props.user.userData.name}
                    </Typography>
                </IconButton>
                <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                    }}
                    open={open}
                    onClose={handleClose}
                >
                    <MenuItem onClick={handleFavourites}  disabled={(props.app.map.postcodes.length < 2 ? false:true)}>My Favourites</MenuItem>
                    <MenuItem onClick={handleDownload} disabled={(props.app.map.postcodes.length > 0 ? false:true)}>Download Selection</MenuItem>
                    <MenuItem onClick={handleLogout} >Logout</MenuItem>
                    
                </Menu>
                </div>
            )}
            </Toolbar>
        </AppBar>
        <Dialog
            open={favouriteStatus}
            onClose={handleCloseFavouriteView}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{"My Favourites"}</DialogTitle>
            <DialogContent>
            
               <FavouriteView closeFavouriteView={handleCloseFavouriteView}></FavouriteView>
           
            </DialogContent>
            <DialogActions>
            <Button onClick={handleCloseFavouriteView} color="primary" autoFocus>
                Ok
            </Button>
            </DialogActions>
        </Dialog>
        </div>
    );
}


export default NavBar