import React from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import styled from 'styled-components';
import axios from 'axios';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

// const getRows = (crimeData) =>{
//     var rows = [];
//     try{
//         crimeData.forEach(data => {
//             const dataObj = data
//             // console.log("GENERAL ", dataObj)
//             rows.push(createData(dataObj["totalCrimes"],dataObj["totalMurders"], dataObj["totalAssault"],dataObj["totalBurgalary"],dataObj["totalTheft"]))
//         })
//     }catch(e){
//         console.log(e)
//         rows = []
//     }
//     return rows
// }

// function createData(totalCrimes,totalMurders, totalAssault,totalBurgalary,totalTheft) {
//     return {totalCrimes,totalMurders, totalAssault,totalBurgalary,totalTheft};
// }

// function createAverage(avgPopulation,avgliviblity) {
//     return {avgPopulation,avgliviblity};
//  }
 
//  function getAverages(data){
//     let avgPopulation = 0;
//     let avgliviblity = 0;
//     if(data.length > 0){
//        data.forEach(element => { 
//             avgPopulation += element['population'];
//             avgliviblity += element['liviblity_score'];
            
//        });

//        avgPopulation = avgPopulation / data.length;
//        avgliviblity = avgliviblity / data.length;
       
//     }
//     return createAverage(avgPopulation,avgliviblity)
//  }

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.info.main,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);
 
const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
        },
    },
}))(TableRow);


const StyledExpansionSummary = withStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.default,
    },
}))(ExpansionPanelSummary);

const useStyles = makeStyles((theme) => ({
    table: {
        marginTop: '5%',
      //minWidth: 700,
      //width: '100%'
    },
    paper: {
       //padding: theme.spacing(2),
       marginLeft: '5%',
       marginRight: '5%',
       marginTop: '2%',
       marginBottom: '2%',
       paddingBottom: '2%',
       textAlign: 'center',
       color: theme.palette.text.secondary 
     },
     container: {
        width: '100%',
        textAlign: 'center',
        overflowX: "auto",
     },
     summary: {
        //padding: theme.spacing(2),
        marginLeft: '1%',
        marginBottom: '2%',
        marginTop: '2%',
        textAlign: 'Left',
        paddingTop: '1%',
        color: theme.palette.text.primary,
        
        },
    summaries: {
        width: '100%',
        marginTop: '2%',
        display: 'flex',
        textAlign: 'center', 
        
    },
    summaryItem: {
        width: '100%',
        textAlign: 'center', 
    }
  }));
var rows;
const crime = (props) => {
    const classes = useStyles();

    const {crimeData} = props.crimeStats
    // const rows = getRows(crimeData)
   //  const averages = getAverages(rows);


    return(
        <Paper className={classes.paper}>
            <div className={classes.container}>
                <Paper className={classes.summary}  elevation={0}>
                    <h3 width="100%">Crime Stats {(props.postcode === undefined ? ' ': ' | ' + props.postcode)} </h3>
                    <div className={classes.summaries}>
                        <Paper className={classes.summaryItem} elevation={0}>
                            <h4>Total Crimes</h4>
                            {/* {console.log(crimeData['totalCrimes'])} */}
                            <Typography className={classes.summaryItem}>{(crimeData['totalCrimes'] === undefined ? ' ':crimeData['totalCrimes'])}</Typography>
                        </Paper>
                    </div>
                    <div className={classes.summaries}>
                        <Paper className={classes.summaryItem} elevation={0}>
                            <h4>Total Murders</h4>
                            <Typography className={classes.summaryItem}>{(crimeData['totalMurders'] === undefined ? ' ':crimeData['totalMurders'])}</Typography>
                        </Paper>
                        <Paper className={classes.summaryItem} elevation={0}>
                            <h4>Total Assaults</h4>
                            <Typography className={classes.summaryItem}>{(crimeData['totalAssault'] === undefined ? ' ':crimeData['totalAssault'])}</Typography>
                        </Paper>
                    </div>
               </Paper>
               <Paper className={classes.summary}  elevation={0}>
                    <div className={classes.summaries}>
                        <Paper className={classes.summaryItem} elevation={0}>
                            <h4>Total Burglaries</h4>
                            {/* {console.log(crimeData['totalCrimes'])} */}
                            <Typography className={classes.summaryItem}>{(crimeData['totalBurgalary'] === undefined ? ' ':crimeData['totalBurgalary'])}</Typography>
                        </Paper>
                        <Paper className={classes.summaryItem} elevation={0}>
                            <h4>Total Theft</h4>
                            <Typography className={classes.summaryItem}>{(crimeData['totalTheft'] === undefined ? ' ':crimeData['totalTheft'])}</Typography>
                        </Paper>

                    </div>
               </Paper>
                
                
            </div>
        </Paper>
    )




}

export default crime;




// <ExpansionPanel className={classes.container}>
// <StyledExpansionSummary
//         aria-controls="panel1a-content"
//         id="panel1a-header"
//         className={classes.container}
//     >
//         <div className={classes.container}>
//         {<ExpandMoreIcon />}
//         </div>
    
//     </StyledExpansionSummary>
//     <ExpansionPanelDetails className={classes.container}>
//         <div className={classes.container}>
//             <Table className={classes.table} aria-label="customized table">
//                 <TableHead>
//                     <TableRow>
//                         <StyledTableCell>Murders</StyledTableCell>
//                         <StyledTableCell>Assaults</StyledTableCell>
//                         <StyledTableCell>Burglaries</StyledTableCell>
//                         <StyledTableCell align="right">Thefts</StyledTableCell>
//                     </TableRow>
//                 </TableHead>
//                 <TableBody>
//                     {rows.map((row) => (
//                         <StyledTableRow key={row.month}>
//                             <StyledTableCell component="th" scope="row">
//                                 {row.suburb}
//                             </StyledTableCell>
//                             <StyledTableCell component="th" scope="row">
//                                 {row.state}
//                             </StyledTableCell>
//                             <StyledTableCell component="th" scope="row">
//                                 {row.population}
//                             </StyledTableCell>
//                             <StyledTableCell align="right">
//                                 {row.median_income}
//                             </StyledTableCell>
//                         </StyledTableRow>
//                     ))}
//                 </TableBody>
//             </Table>
        
//         </div>
//     </ExpansionPanelDetails>
// </ExpansionPanel>