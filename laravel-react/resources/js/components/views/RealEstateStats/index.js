import React from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import styled from 'styled-components';
import axios from 'axios';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUp, faArrowDown} from '@fortawesome/free-solid-svg-icons';

import { element } from 'prop-types';

const getRows = (generalData) =>{
    var rows = [];
    try{
        generalData.forEach(data => {
            const dataObj = data
            // console.log("GENERAL ", dataObj)
            rows.push(createData(dataObj["id"],dataObj["suburb"],dataObj["median_house_price"], dataObj["price_movement_five_years"],dataObj["2_bedroom_price_avg"],dataObj["3_bedroom_price_avg"],dataObj["4_bedroom_price_avg"],dataObj["liviblity_score"]))
        })
    }catch(e){
        console.log(e)
        rows = []
    }
    return rows
}

function createData(id,suburb,median_house_price,price_movement_five_years,avg_2_bed,avg_3_bed,avg_4_bed,liviblity_score) {
    return {id,suburb,median_house_price,price_movement_five_years,avg_2_bed,avg_3_bed,avg_4_bed,liviblity_score};
}

function createAverage(avgPrice,avgMovement,avgAvg2bed,avgAvg3bed,avgAvg4bed,avgliviblity) {
   return {avgPrice,avgMovement,avgAvg2bed,avgAvg3bed,avgAvg4bed,avgliviblity};
}

function format(value){
    formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    });
    content = formatter.format(value);
    return content; 
  }
  

function getAverages(data){
   let avgPrice = 0;
   let avgMovement = 0;
   let avgAvg2bed = 0;
   let avgAvg3bed = 0;
   let avgAvg4bed = 0;
   let avgliviblity = 0;
   if(data.length > 0){
      data.forEach(element => { 
         avgPrice += parseInt(element['median_house_price']);
         avgMovement += parseInt(element['price_movement_five_years']);
         avgAvg2bed += parseInt(element['avg_2_bed']);
         avgAvg3bed += parseInt(element['avg_3_bed']);
         avgAvg4bed += parseInt(element['avg_4_bed']);
         avgliviblity += element['liviblity_score'];
      });
      avgPrice = avgPrice / data.length;
      avgMovement = avgMovement  / data.length;
      avgAvg2bed = avgAvg2bed / data.length;
      avgAvg3bed = avgAvg3bed / data.length;
      avgAvg4bed = avgAvg4bed / data.length;
      avgliviblity = avgliviblity / data.length;
   }
   return createAverage(avgPrice,avgMovement,avgAvg2bed,avgAvg3bed,avgAvg4bed,avgliviblity)
}

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.info.main,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);
 
const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
        },
    },
}))(TableRow);


const StyledExpansionSummary = withStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.default,
    },
}))(ExpansionPanelSummary);

const useStyles = makeStyles((theme) => ({
    table: {
        marginTop: '5%',
      //minWidth: 700,
      width: '100%'
    },
    paper: {
       //padding: theme.spacing(2),
       marginLeft: '5%',
       marginRight: '5%',
       marginTop: '2%',
       textAlign: 'center',
       color: theme.palette.text.secondary 
     },
     container: {
        width: '100%',
        textAlign: 'center',
        overflowX: "auto",
     },
     summary: {
        //padding: theme.spacing(2),
        marginLeft: '1%',
        marginBottom: '2%',
        marginTop: '2%',
        textAlign: 'Left',
        paddingTop: '1%',
        color: theme.palette.text.primary,
        
        },
    summaries: {
        width: '100%',
        marginTop: '2%',
        display: 'flex',
        textAlign: 'center', 
        
    },
    summaryItem: {
        width: '100%',
        textAlign: 'center', 
    },
    hidden:{
        display:"none",
    }
    
  }));
var rows;
const RealEstateStats = (props) => {
    const classes = useStyles();
    const {generalData} = props.generalStats

    const rows = getRows(generalData)
    const averages = getAverages(rows);


   //  console.log(typeofaverages.avgPrice)
    

   var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });
  
//   formatter.format(2500); /* $2,500.00 */
    

    return(
        
        <Paper className={classes.paper}>
            
            <div className={classes.container}>
               <Paper className={classes.summary}  elevation={0}>
               <h3 width="100%">Real Estate Stats {(props.postcode === undefined ? ' ': ' | ' + props.postcode)} </h3>
                  <div className={classes.summaries}>
                     <Paper className={classes.summaryItem} elevation={0}>
                        <h4>Average Price</h4> 
                        <Typography className={classes.summaryItem}>{(averages.avgPrice === 0 ? ' ': formatter.format(averages.avgPrice) )}</Typography>
                     </Paper>
                     <Paper className={classes.summaryItem} elevation={0}>
                        <h4>Average Price Movement</h4>
                        <Typography className={classes.summaryItem}>
                            
                            <span  className={ averages.avgMovement === 0 ? classes.hidden : ''}>
                                <FontAwesomeIcon icon={(averages.avgMovement >= 0) ? faArrowUp : faArrowDown} />
                            </span>
                            
                            {(averages.avgMovement === 0 ? ' ': ' ' + averages.avgMovement)}
                        </Typography>
                     </Paper>
                     
                  </div>
                  
               </Paper>
                <ExpansionPanel className={classes.container}>
                <StyledExpansionSummary
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                        className={classes.container}
                    >
                        <div className={classes.container}>
                        {<ExpandMoreIcon />}
                        </div>
                    
                    </StyledExpansionSummary>

                    <ExpansionPanelDetails className={classes.container}>
                        <div className={classes.container}>
                            
                            <div style={{textAlign: 'left', paddingLeft: '3%'}}>
   
                            </div>
                            <Table className={classes.table} aria-label="customized table">
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell align="center">Suburb</StyledTableCell>
                                        <StyledTableCell align="center">Median House Price</StyledTableCell>
                                        <StyledTableCell align="center">2 Bed Average</StyledTableCell>
                                        <StyledTableCell align="center">3 Bed Average</StyledTableCell>
                                        <StyledTableCell align="center">4 Bed Average</StyledTableCell>
                                        <StyledTableCell align="center">Price Movement (5 Years)</StyledTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.map((row) => (
                                        <StyledTableRow key={row.id}>
                                            <StyledTableCell component="th" scope="row" align="center">
                                                {row.suburb}
                                            </StyledTableCell>
                                            <StyledTableCell component="th" scope="row" align="center">
                                                {formatter.format(row.median_house_price)}
                                            </StyledTableCell>
                                            <StyledTableCell component="th" scope="row" align="center">
                                                {formatter.format(row.avg_2_bed)}
                                            </StyledTableCell>
                                            <StyledTableCell component="th" scope="row"  align="center">
                                                {formatter.format(row.avg_3_bed)}
                                            </StyledTableCell>
                                            <StyledTableCell align="center">
                                                {formatter.format(row.avg_4_bed)}
                                            </StyledTableCell>
                                            <StyledTableCell align="center">
                                            <span  className={ row.price_movement_five_years === 0 ? classes.hidden : ''}>
                                               <FontAwesomeIcon icon={(row.price_movement_five_years >= 0) ? faArrowUp : faArrowDown} />
                                            </span>
                                            {" "+row.price_movement_five_years}
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        <Paper className={classes.paper} elevation={0} >
                           <div className={classes.container}>
                              Real Estate Statistics are generated for testing purposes.
                           </div>
                        </Paper>
                        </div>
                        
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        </Paper>
    )




}

export default RealEstateStats;
