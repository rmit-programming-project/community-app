import React from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import styled from 'styled-components';
import axios from 'axios';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import FavouriteIcon from '../FavouriteIcon/favouriteIcon';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const getRows = (generalData) =>{
    var rows = [];
    try{
        generalData.forEach(data => {
            const dataObj = data
            // console.log("GENERAL ", dataObj)
            rows.push(createData(dataObj["id"],dataObj["suburb"], dataObj["state"],dataObj["population"],dataObj["median_income"],dataObj["liviblity_score"],dataObj["sqkm"],dataObj["elevation"],dataObj["timezone"],dataObj["local_goverment_area"]))
            
        })
    }catch(e){
        console.log(e)
        rows = []
    }
    return rows
}

function createData(id,suburb, state,population,median_income,liviblity_score,sqkm,altitude,timezone,local_goverment_area) {
    return {id,suburb, state,population,median_income,liviblity_score,sqkm,altitude,timezone,local_goverment_area};
}

function createAverage(avgPopulation,avgliviblity) {
    return {avgPopulation,avgliviblity};
 }
 
 function getAverages(data){
    let avgPopulation = 0;
    let avgliviblity = 0;
    if(data.length > 0){
       data.forEach(element => {
            
            avgPopulation += element['population'];
            avgliviblity += element['liviblity_score'];
            
       });

       avgPopulation = avgPopulation / data.length;
       avgliviblity = avgliviblity / data.length;
       
    }
    return createAverage(avgPopulation,avgliviblity)
 }

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.info.main,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);
 
const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
        },
    },
}))(TableRow);


const StyledExpansionSummary = withStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.default,
    },
}))(ExpansionPanelSummary);

const useStyles = makeStyles((theme) => ({
    table: {
        marginTop: '5%',
      //minWidth: 700,
      width: '100%'
    },
    paper: {
       //padding: theme.spacing(2),
       marginLeft: '5%',
       marginRight: '5%',
       marginTop: '2%',
       textAlign: 'center',
       color: theme.palette.text.secondary 
     },
     container: {
        width: '100%',
        textAlign: 'center',
        overflowX: "auto",
     },
     summary: {
        //padding: theme.spacing(2),
        marginLeft: '1%',
        marginBottom: '2%',
        marginTop: '2%',
        textAlign: 'Left',
        paddingTop: '1%',
        color: theme.palette.text.primary,
        
        },
    summaries: {
        width: '100%',
        marginTop: '2%',
        display: 'flex',
        textAlign: 'center', 
        
    },
    summaryItem: {
        width: '100%',
        textAlign: 'center', 
    }
  }));
var rows;
const general = (props) => {
    const classes = useStyles();
    const {generalData} = props.generalStats
    const {transportData} = props.transportStats

    const rows = getRows(generalData)
    const averages = getAverages(rows);


    return(
        <Paper className={classes.paper}>
            <div className={classes.container}>
                <Paper className={classes.summary}  elevation={0}>
                    <h3 width="100%">General Stats {(props.postcode === undefined ? ' ': ' | ' + props.postcode)}  {(rows[0] === undefined ? ' ': ' | ' + rows[0].state)} </h3>
                    <FavouriteIcon generalData={generalData} postcode={props.postcode}></FavouriteIcon>
                    <div className={classes.summaries}>
                        <Paper className={classes.summaryItem} elevation={0}>
                            <h4>Livability Score</h4>
                            <Typography className={classes.summaryItem}>{(averages.avgliviblity === 0 ? ' ':(averages.avgliviblity).toFixed(0))}</Typography>
                        </Paper>
                        <Paper className={classes.summaryItem} elevation={0}>
                            <h4>Average Population</h4>
                            <Typography className={classes.summaryItem}>{(averages.avgPopulation === 0 ? ' ':(averages.avgPopulation).toFixed(0))}</Typography>
                            
                        </Paper>

                    </div>
                    <div className={classes.summaries}>
                        <Paper className={classes.summaryItem} elevation={0}>
                            <h4>Bus / Train / Tram Stops</h4>
                            <Typography className={classes.summaryItem}>{(transportData.totalCount === 0 ? ' Not Found ':transportData.totalCount)}</Typography>
                        </Paper>
                    </div>
               </Paper>
                
                
                <ExpansionPanel className={classes.container}>
                <StyledExpansionSummary
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                        className={classes.container}
                    >
                        <div className={classes.container}>
                        {<ExpandMoreIcon />}
                        </div>
                    
                    </StyledExpansionSummary>
                    <ExpansionPanelDetails className={classes.container}>
                        <div className={classes.container}>
                            
                            <div style={{textAlign: 'left', paddingLeft: '3%'}}>
                                {/* <Typography>
                                    Number of Schools: {generalData.length}
                                </Typography> */}
                            </div>
                            <Table className={classes.table} aria-label="customized table">
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell align="center">Suburb Name</StyledTableCell>
                                        <StyledTableCell align="center">Local Goverment</StyledTableCell>
                                        <StyledTableCell align="center">Population</StyledTableCell>
                                        <StyledTableCell align="center">Median Income</StyledTableCell>
                                        <StyledTableCell align="center">Area (sqkm)</StyledTableCell>
                                        <StyledTableCell align="center">Altitude</StyledTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.map((row, key) => (
                                        <StyledTableRow key={key}>
                                            <StyledTableCell component="th" scope="row" align="center">
                                                {row.suburb}
                                            </StyledTableCell>
                                            <StyledTableCell component="th" scope="row" align="center">
                                                {row.local_goverment_area}
                                            </StyledTableCell>
                                            <StyledTableCell component="th" scope="row" align="center">
                                                {row.population}
                                            </StyledTableCell>
                                            <StyledTableCell align="center">
                                                {row.median_income}
                                            </StyledTableCell>
                                            <StyledTableCell align="center">
                                                {row.sqkm}
                                            </StyledTableCell>
                                            <StyledTableCell align="center">
                                                {row.altitude}
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        </Paper>
    )




}

export default general;