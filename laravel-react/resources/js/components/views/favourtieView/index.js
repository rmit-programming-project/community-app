import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { connect } from "react-redux"
import {updateMap, setPostcodes} from "../../stores/app/actions"

import FavouriteView from './favouriteView'


class FavouritePage extends Component {

    constructor(props){
        super(props)
        
    }

    selectFavourite = (data) => {
        let input = data.postcode.toString()
        // console.log("FAVOURTIE SELECTED", data)
        
        const postcodeList = this.props.postcodes
        this.props.closeFavouriteView()
        
        if(postcodeList.length < 2){
          if(!postcodeList.includes(input)){
            // console.log(typeof(input))
            // console.log(typeof(postcodeList[0]))

            postcodeList.push(input)
            // console.log("POST CODE LIST = ", postcodeList)
            this.props.setPostcodes(postcodeList)
            this.props.updateMap(input)
          }
        }
        
        
    }

    render(){
        return(
            <FavouriteView favourites={this.props.favourites} selectFavourite={this.selectFavourite}></FavouriteView>
        )
    }
}


export const mapStateToProps = store => {
    return {
        app: store.app,
        favourites: store.app.user.favourites,
        postcodes: store.app.map.postcodes
    };
};

export const mapDispatchToProps = dispatch =>{
    return {
      updateMap: async (data) => {
        dispatch(updateMap(data));
      },
      setPostcodes: async (data) => {
        dispatch(setPostcodes(data))
      }
  
    };
  }


export default connect(mapStateToProps, mapDispatchToProps)(FavouritePage)