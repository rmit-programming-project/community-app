import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { withStyles, makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    paper: {
       //padding: theme.spacing(2),
       marginLeft: '5%',
       marginRight: '5%',
       marginTop: '2%',
       textAlign: 'center',
       color: theme.palette.text.secondary 
     },
    
  }));

  

const FavouriteView = (props) => {
    const classes = useStyles();



    const favourites = props.favourites.map((element) => <Paper onClick={() => props.selectFavourite(element)} className={classes.paper} key={element.id}>{element.postcode}</Paper>)
    
    return(
        <div>{favourites}</div>
    )
}

export default FavouriteView