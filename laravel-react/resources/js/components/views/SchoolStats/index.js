import React from 'react';
import 'leaflet/dist/leaflet.css';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';

const getRows = (schoolData) =>{
    var rows = [];
    try{
        schoolData.forEach(school => {
            const schoolObj = school
            // console.log("SCHOOL ", schoolObj)
            if(schoolObj[1][0] == undefined){
                rows.push(createData(schoolObj[0]["school_name"], schoolObj[0]["school_type"],"Not Found","Not Found","Not Found","Not Found"))
            }else{
                rows.push(createData(schoolObj[0]["school_name"], schoolObj[0]["school_type"],schoolObj[1][0]["girls-enrolments"],schoolObj[1][0]["boys-enrolments"],schoolObj[1][0]["indigenous-enrolments"],schoolObj[1][0]["icsea"]))
            }
        })
    }catch(e){
        console.log(e)
        rows = []
    }
    return rows
}

function createData(name, type,girls,boys,indigenous,icsea) {
    return {name, type,girls,boys,indigenous,icsea};
}

function createAverage(avgicsea) {
    return {avgicsea};
 }
 
 function getAverages(data){
    // console.log("Start getting averages");
    let dataavailable = 0;
    let avgicsea = 0;
    
    if(data.length > 0){
       data.forEach(school => {
            if (school[1][0] === undefined) {
                return;
            }
            avgicsea += school[1][0]['icsea'];

            dataavailable++;
            
       });

       avgicsea = avgicsea / dataavailable;
       
    }
    // console.log("Stop getting averages");
    return createAverage(avgicsea)
 }

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.info.main,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);
 
const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
        },
    },
}))(TableRow);

const StyledExpansionSummary = withStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.default,
    },
}))(ExpansionPanelSummary);

const useStyles = makeStyles((theme) => ({
    table: {
        marginTop: '5%',
        minWidth: "100%",


    },
    paper: {
       //padding: theme.spacing(2),
       marginLeft: '5%',
       marginRight: '5%',
       marginTop: '2%',
       textAlign: 'center',
       color: theme.palette.text.secondary 
     },
     container: {
        width: '100%',
        textAlign: 'center',
        overflowX: "auto",
     },
     summary: {
        //padding: theme.spacing(2),
        marginLeft: '1%',
        marginBottom: '2%',
        marginTop: '2%',
        textAlign: 'Left',
        paddingTop: '1%',
        color: theme.palette.text.primary,
        
        },
    summaries: {
        width: '100%',
        marginTop: '2%',
        display: 'flex',
        textAlign: 'center', 
        
    },
    summaryItem: {
        width: '100%',
        textAlign: 'center', 
    },


  }));
var rows;
const school = (props) => {
    const classes = useStyles();
    const {schoolData} = props.schoolStats
    // console.log("School stats = ", schoolData)
    // console.log("ROWS = ", getRows(schoolData))
    const rows = getRows(schoolData)
    // console.log(rows);
    const averages = getAverages(schoolData)
    

    return(
        <Paper className={classes.paper}>
            <div className={classes.container}>

            <Paper className={classes.summary}  elevation={0}>
                    <h3 width="100%">School Stats {(props.postcode === undefined ? ' ': ' | ' + props.postcode)} </h3>
                    <div className={classes.summaries}>
                        <Paper className={classes.summaryItem} elevation={0}>
                             <h4>Number of Schools</h4>
                            <Typography className={classes.summaryItem}>{(schoolData.length === 0 ? ' ':schoolData.length)}</Typography>
                        </Paper>
                        <Paper className={classes.summaryItem} elevation={0}>
                            <h4>Average ICSEA</h4> 
                            <Typography className={classes.summaryItem}>{(averages.avgicsea === 0 ? ' ':(averages.avgicsea).toFixed(0))}</Typography>
                        </Paper>

                    </div>
               </Paper>
                

                <ExpansionPanel className={classes.container}>
                    <StyledExpansionSummary
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                        className={classes.container}
                    >
                        <div className={classes.container}>
                        {<ExpandMoreIcon />}
                        </div>
                    
                    </StyledExpansionSummary>
                    <ExpansionPanelDetails className={classes.container}>
                        <div className={classes.container}>
                            <Table className={classes.table} aria-label="customized table">
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell align="center">School Name</StyledTableCell>
                                        <StyledTableCell align="center">School Type</StyledTableCell>
                                        <StyledTableCell align="center">Male/Female Students</StyledTableCell>
                                        <StyledTableCell align="center"> ICSEA Ranking</StyledTableCell>
                                        <StyledTableCell align="center"> Indigenous enrolments</StyledTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.map((row, key) => (
                                        <StyledTableRow key={key}>
                                            <StyledTableCell component="th" align="center" scope="row">
                                                {row.name}
                                            </StyledTableCell>
                                            <StyledTableCell align="center">{row.type}</StyledTableCell>
                                            <StyledTableCell align="center">{row.boys + " / " + row.girls}   </StyledTableCell>
                                            <StyledTableCell align="center">{row.icsea}</StyledTableCell>
                                            <StyledTableCell align="center">{row.indigenous}</StyledTableCell>
                                        </StyledTableRow>
                                    ))}
                                </TableBody>
                            </Table>
                            <Paper className={classes.paper} elevation={0} >
                           <div className={classes.container}>
                              ICSEA = Index of Community  Socio-Educational Advantage.
                           </div>
                           <div className={classes.container}>
                              Higher ICSEA means more advantages
                           </div>
                        </Paper>
                        
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        </Paper>
    )




}

export default school;