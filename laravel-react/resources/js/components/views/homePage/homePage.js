
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { connect } from "react-redux"
import Header from '../../Header'

import Map from '../Map/index.js'
import SuburbList from '../SuburbList/index.js'
import CrimeStats from '../CrimeStats/index.js'
import RealEstateStats from '../RealEstateStats/index.js'
import Weather from '../Weather/index.js'
import School from '../SchoolStats/index'
import General from '../GeneralStats/index'

import Card from '@material-ui/core/Card';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import { color } from '@material-ui/system'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles((theme)=> ({
    root: {
      flexGrow: 1,
      height: '100%',
      minHeight: '100vh',
    },
    paper: {
      padding: theme.spacing(2),
      marginLeft: '5%',
      marginRight: '5%',
      marginTop: '2%',
      textAlign: 'center',
      color: theme.palette.text.secondary,
      minHeight: "80px",
      
    },
    papermap: {
      marginLeft: '5%',
      marginRight: '5%',
      marginTop: '2%',
      textAlign: 'center',
      color: theme.palette.text.secondary
    }
  }));


const HomePage = props => {

    
    const classes = useStyles();
    
    const {appName, weatherStats, schoolStats, map, removeSearch, generalStats, logout, crimeStats, transportStats,loaded,invalid} = props;
    // console.log(transportStats)
    const submitLogout = () => {
      // console.log("logging in")
      logout();
    }
    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };
    
        
        return (
          <div className={classes.root}>
          
        
              <Grid container spacing={0}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Header name={appName}/>
                  </Paper>
                </Grid>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <SuburbList removeSearch={removeSearch} map={map} loaded={loaded} invalid={invalid} />
                  </Paper>
                </Grid>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Map/> 
                  </Paper>
                </Grid>
                <Grid item xs={(map.postcodes.length <= 1)? 12 : 6}>
                  <General generalStats={generalStats.primary} transportStats={transportStats.primary} postcode={map.postcodes[0]}/>
                </Grid>
                <Grid item xs={6} style={{display: (map.postcodes.length>1)? 'block':'none'}}>
                  <General style={{display: (map.postcodes.length>1)? 'block':'none'}} generalStats={generalStats.secondary} transportStats={transportStats.secondary} postcode={map.postcodes[1]}/>
                </Grid>

                <Grid item xs={(map.postcodes.length <= 1)? 12 : 6}>
                  <School schoolStats={schoolStats.primary} postcode={map.postcodes[0]}/>
                </Grid>
                <Grid item xs={6} style={{display: (map.postcodes.length>1)? 'block':'none'}}>
                  <School style={{display: (map.postcodes.length>1)? 'block':'none'}} schoolStats={schoolStats.secondary} postcode={map.postcodes[1]}/>
                </Grid>


                <Grid item xs={(map.postcodes.length <= 1)? 12 : 6}>
                  <Weather weatherStats={weatherStats.primary} postcode={map.postcodes[0]}/>
                </Grid> 
                <Grid item xs={6} style={{display: (map.postcodes.length>1)? 'block':'none'}}>
                  <Weather style={{display: (map.postcodes.length>1)? 'block':'none'}} weatherStats={weatherStats.secondary} postcode={map.postcodes[1]}/>
                </Grid>

                <Grid item xs={(map.postcodes.length <= 1)? 12 : 6}>
                  <RealEstateStats generalStats={generalStats.primary} postcode={map.postcodes[0]}/>
                </Grid>
                <Grid item xs={6} style={{display: (map.postcodes.length>1)? 'block':'none'}}>
                  <RealEstateStats style={{display: (map.postcodes.length>1)? 'block':'none'}} generalStats={generalStats.secondary} postcode={map.postcodes[1]}/>
                </Grid>

                <Grid item xs={(map.postcodes.length <= 1)? 12 : 6}>
                  <CrimeStats crimeStats={crimeStats.primary} postcode={map.postcodes[0]}/>
                </Grid>
                <Grid item xs={6} style={{display: (map.postcodes.length>1)? 'block':'none'}}>
                  <CrimeStats style={{display: (map.postcodes.length>1)? 'block':'none'}} crimeStats={crimeStats.secondary} postcode={map.postcodes[1]}/>
                </Grid>
              </Grid>
          </div>
        )
      
          
}


// <Grid item xs={12}>
//                   <School schoolStats={schoolStats.primary}></School>
//                 </Grid>
// <Grid item xs={12}>
// <Weather weatherStats={weatherStats}/>
// </Grid> 
                        // --in--       --out--
  export default HomePage;

  