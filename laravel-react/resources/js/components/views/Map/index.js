import React from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import styled from 'styled-components';
import { connect } from "react-redux";
import { element } from 'prop-types';
import axios from 'axios';

const Wrapper = styled.div`
   width: ${props => props.width};
   height: ${props => props.height};
`;

//The purpose of this component is to handle all maping for the application. 

export class Map extends React.Component {


   basemap = L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png', { 
         api_key: 'c9dc4a54-facf-46be-9ece-dcd26b2bd4dc',
      });
   map;

   componentDidMount() {

      
      // var marker = new L.Marker(map.getCenter(), {icon : icon}).addTo(map);
      
      const {postcode} = this.props


      

      this.map = L.map('map', {
         zoom: 4,
         maxZoom: 15,
         minZoom: 4,
         zoomControl: true,
         scrollWheelZoom: false,
         layers: [ this.basemap ]
      });

      
      this.map.setView([-25.2744, 133.7751]);
      // console.log(this.map['_layers']);

   }


   shouldComponentUpdate() {
      const{response} = this.props;
      if(response['primary']['mapData'].length == 0){
         this.map.flyTo([-25.2744, 133.7751],4);
         this.map.eachLayer(function(layer){
            // console.log("clear map");
            if(layer['_url'] == undefined){
               layer.remove();
            }
         });

      }
      if(this.props){
         return true;
      }else{
         return false;
      }
   }

   render() {

      const{response} = this.props;
      const{store} = this.props;
      var shadowlessIcon = new L.Icon.Default();
      shadowlessIcon.options.shadowSize = [0,0];

      // console.log(store);

      
      if(response['primary']['mapData'].length > 0 || response['secondary']['mapData'].length>0){
         

         let layerGroup = L.featureGroup();
         

         response['primary']['mapData'].forEach((location)=>{
            L.marker([location.latitude,location.longitude],{icon: shadowlessIcon}).addTo(layerGroup)
         })

         response['secondary']['mapData'].forEach((location)=>{
            L.marker([location.latitude,location.longitude],{icon: shadowlessIcon}).addTo(layerGroup)
         })
         
         
         this.map.eachLayer(function(layer){
            // console.log("clear map");
            if(layer['_url'] == undefined){
               layer.remove();
            }
         });
         
         // console.log(layerGroup.getBounds())
         // this.map.addLayer(this.basemap);
         this.map.addLayer(layerGroup);
         // this.map.getLayers();

         this.map.flyToBounds(layerGroup.getBounds(),{
            maxZoom:13,
         });


      }


      
      return (
         <div>
            <Wrapper width="100%" height="500px" id="map" />
            
         </div>
      );
   }

}

export const mapStateToProps = store => {
   return {
      postcode: store.app.map.postcode,
      response: store.app.map,
      store: store


   }
}



export default connect(mapStateToProps, null)(Map)

