
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { connect } from "react-redux"
import HomePage from '../../views/homePage/homePage';
import NavBar from '../../views/nav-bar/nav-bar'
import {
  setPostcodes, 
  removePrimarySchoolData, 
  removeSecondarySchoolData, 
  removePrimaryWeatherData, 
  removeSecondaryWeatherData,
  removePrimaryMapData, 
  removeSecondaryMapData,
  removePrimaryGeneralData, 
  removeSecondaryGeneralData, 
  setUserData,
  removePrimaryCrimeData,
  removeSecondaryCrimeData,
  logout,
  removePrimaryTransportData,
  removeSecondaryTransportData,
  
  

} from '../../stores/app/actions'
class Home extends Component {

  shouldComponentUpdate() {
      return true;
  }
  
  constructor(props) {
    super(props);
  }

  login = () => {
    console.log("LOGIN HELLO");
    this.props.loginUser("hello");
  }

  logout = () => {
    this.props.logout()
  }


  removeSearch = (postcode) => {
    console.log("removing search ", postcode)
    //[0,1]
    if(this.props.map.postcodes.length == 2){
      if(this.props.map.postcodes[0] == postcode){
        this.props.removePrimaryMapData(postcode)
        this.props.removePrimarySchoolData(postcode)
        this.props.removePrimaryWeatherData(postcode)
        this.props.removePrimaryGeneralData(postcode)
        this.props.removePrimaryCrimeData(postcode)
        this.props.removePrimaryTransportData(postcode)
      }else{
        this.props.removeSecondaryMapData(postcode)
        this.props.removeSecondarySchoolData(postcode)
        this.props.removeSecondaryWeatherData(postcode)
        this.props.removeSecondaryGeneralData(postcode)
        this.props.removeSecondaryCrimeData(postcode)
        this.props.removeSecondaryTransportData(postcode)
      }
    }else if (this.props.map.postcodes.length == 1){
      this.props.removePrimaryMapData(postcode)
      this.props.removePrimarySchoolData(postcode)
      this.props.removePrimaryWeatherData(postcode)
      this.props.removePrimaryGeneralData(postcode)
      this.props.removePrimaryCrimeData(postcode)
      this.props.removePrimaryTransportData(postcode)
    }
    const postcodes = this.props.map.postcodes.filter(x => x != postcode);
    console.log(postcodes)
    this.props.setPostcodes(postcodes)
  }
  
    
  render () {
      //this is getting user data passed in from props
      
      const {weatherStats, schoolStats, generalStats, map,crimeStats, transportStats, loaded, invalid} = this.props;
      
      return (
        
        <div>
          <NavBar logout={this.logout} user={this.props.app.user} app={this.props.app}></NavBar>
          <HomePage weatherStats={weatherStats} schoolStats={schoolStats} map={map} removeSearch={this.removeSearch} generalStats={generalStats} crimeStats={crimeStats} transportStats={transportStats} loaded={loaded} invalid={invalid} logout={this.logout}></HomePage>

        </div>
        
      )
    }
          
}

export const mapStateToProps = store => {
    return {
      app: store.app,
      loaded: store.app.loaded,
      invalid: store.app.invalid,
      map: store.app.map,
      weatherStats: store.app.weather,
      schoolStats: store.app.school,
      generalStats: store.app.general,
      crimeStats: store.app.crime,
      transportStats: store.app.transport,
    };
  };
  export const mapDispatchToProps = dispatch =>{
    return {
      setPostcodes: (data) => {
        dispatch(setPostcodes(data));
      },
      removePrimaryMapData: (data) => {
        dispatch(removePrimaryMapData(data));
      },
      removeSecondaryMapData: (data) => {
        dispatch(removeSecondaryMapData(data));
      },
      removePrimarySchoolData: (data) => {
        dispatch(removePrimarySchoolData(data));
      },
      removeSecondarySchoolData: (data) => {
        dispatch(removeSecondarySchoolData(data));
      },
      removePrimaryWeatherData: (data) => {
        dispatch(removePrimaryWeatherData(data));
      },
      removeSecondaryWeatherData: (data) => {
        dispatch(removeSecondaryWeatherData(data));
      },
      removePrimaryGeneralData: (data) => {
        dispatch(removePrimaryGeneralData(data));
      },
      removeSecondaryGeneralData: (data) => {
        dispatch(removeSecondaryGeneralData(data));
      },
      removePrimaryCrimeData: (data) => {
        dispatch(removePrimaryCrimeData(data));
      },
      removeSecondaryCrimeData: (data) => {
        dispatch(removeSecondaryCrimeData(data));
      },
      removePrimaryTransportData: (data) => {
        dispatch(removePrimaryTransportData(data));
      },
      removeSecondaryTransportData: (data) => {
        dispatch(removeSecondaryTransportData(data));
      },
      setUserData: (data) => {
        dispatch(setUserData(data));
      },
      logout: () => {
        dispatch(logout());
      },
  
    };
  }
                        // --in--       --out--
export default connect(mapStateToProps, mapDispatchToProps)(Home);
