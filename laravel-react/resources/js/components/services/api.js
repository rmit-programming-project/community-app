import axios from 'axios';
import formData from 'form-data';


export const login = async (email, password) => {
   // console.log('inside longing api')
   var form = new formData();
   form.append('email', email)
   form.append('password', password)
   return new Promise((resolve, reject)=>{
      axios.post(`/api/user/login`, form)
       .then(resp => {
         if(resp.status == 200){
            resolve( new Promise((res, rej)=>{
               axios.post(`/login`, form).then((data)=>{
                  res(resp);
               })
            }))
         }else{
            res(resp)
         }
       })
  })   
}


export const registerUser = (name, email, psw) => {
   // console.log('inside register api')
   var form = new formData();
   form.append('email', email)
   form.append('password', psw)
   form.append('name', name)
   return new Promise((resolve, reject)=>{
     axios.post(`/api/register`, form)
      .then(resp => {
         // console.log("resp = ", resp)
         if(resp.status == 200 || resp.status == 201){
            resolve( new Promise((res, rej)=>{
               axios.post(`/login`, form).then((data)=>{
                 res(resp);
               })
            }))
         }else{
            res(resp)
         }
      })
   })   
 }

export const getFavourites = async (userId) => {
   return new Promise((res, rej)=>{
      axios.get(`favorite-suburbs/${userId}`).then((resp)=>{
         // console.log("Got favourites =", resp)
         if(!resp.data.message){
            res(resp.data)
         }else{
            res([])
         }
         
       })
   })
}

export const addFavourite = async (suburbid) => {
   return new Promise((res, rej)=>{
      axios.post(`add-suburb-to-favourites/${suburbid}`).then((resp)=>{
         // console.log("FAVOURITES =", resp)
         if(resp.status == 200){
            // console.log("Successfully added Favourite")
            res();
         }else{
            console.log("Failed to add favourite")
            rej();
         }  
       }).catch((err)=>{
          console.log("Failed to add favourite")
          rej();
       })
   })
   
}



 
export const logout = async () => {
   // console.log("logging out")
   return new Promise((resolve, reject)=>{
      axios.post(`/logout`).then(res => {
         resolve(res);
      })
   })
   
}


export const updateMapLocation = async (postcode) => {
    return new Promise((resolve, reject)=>{
        axios.get(`/api/details?postcode=${postcode}`)
         .then(res => {
            
            let test = [];
            if(res.status == 200){
            res.data.forEach(element => {

               let item = {
                  location: element['location'],
                  postcode: element['postcode'],
                  state: element['state'],
                  latitude: element['latitude'],
                  longitude: element['longitude']
               }
               test.push(item);
            });
         

            // this.setState({ response: test });
            resolve({response: test})
         }
            // { console.log(this.state.response) }

         }).catch(error => {reject(error)});
    })   
}
//       WEATHER #################################################################
export const getMonthlyWeatherAverage = async (lat, long) =>{

   return new Promise((res, rej)=>{
      axios.get(`/api/monthly-data?lat=${lat}&lng=${long}`)
         .then(resp => {
            // console.log(`/api/monthly-data?lat=${lat}&lng=${long}`)
            // console.log(resp.data);
            res(resp.data)
         })
   })
}

export const getWeatherNow = async(lat, long) => {
   const latitude = -37.81768353
   const longitude = 145.1077782
   return new Promise((res, rej)=>{ 
      axios.get(`/api/suburb-data?lat=${lat}&lng=${long}`)
      .then(resp => { 
         res(resp.data.current)
      })
   })
}

export const getAirQuality = async(postcode) => {
   const latitude = -37.81768353
   const longitude = 145.1077782
   return new Promise((res, rej)=>{ 
      axios.get(`/api/air-quality?postcode=${postcode}`)
      .then(resp => { 
         res(resp.data)
      })
   })
}



//       SCHOOLS #################################################################
export const getSchoolDataByPostcode = async(postcode) => {
   return new Promise((res, rej) => {
      axios.get(`/api/schools/postcode/${postcode}`).then(resp => {
         res(resp.data)
      })
   })
}



//       GENERAL #################################################################
export const getGeneralDataByPostcode = async(postcode) => {
   
   return new Promise((res, rej) => {
      axios.get(`/api/suburb/postcode/${postcode}`).then(resp => {
         res(resp.data)
      })
   })
}

//       CRIME #################################################################
export const getCrimeData = async(postcode) => {
   // console.log("CRIME");
   return new Promise((res, rej) => {
      axios.get(`/api/crime/${postcode}`).then(resp => {
         // console.log("CRIME");
         // console.log(resp.data);
         res(resp.data)
      })
   })
}


//       TRANSPORT #################################################################
export const getTransportData = async(postcode) => {
   // console.log("Transport");
   return new Promise((res, rej) => {
      axios.get(`/api/transport/${postcode}`).then(resp => {
         // console.log("transport");
         // console.log(resp.data);
         res(resp.data)
      })
   })
}





//       EXPORTS #################################################################
export const API = {
    updateMapLocation,
    getMonthlyWeatherAverage,
    getAirQuality,
    getWeatherNow,
    getSchoolDataByPostcode,
    getGeneralDataByPostcode,
    login,
    logout,
    registerUser,
    getFavourites,
    addFavourite,
    getCrimeData,
    getTransportData,
   
}
