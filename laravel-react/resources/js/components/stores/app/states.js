export const initialState = {
  loaded: true,
  invalid: false,
  user: {
    isLoggedIn: false,
    userData: {},
    favourites: []
  },
  map: {
    postcode: '',
    postcodes: [],
    primary: {
      mapData: [],
    },    
    secondary: {
      mapData: [],
    }
  },

  
  school: {
    primary: {
      schoolData: []
    },
    secondary: {
      schoolData: []
    }
  },


  weather: {
    primary: {
      currentWeather: {},
      averageWeather: [],
      airquality: []
    },
    secondary: {
      currentWeather: {},
      averageWeather: [],
      airquality: []
    }
  },

  general: {
    primary: {
      generalData: []
    },
    secondary: {
      generalData: []
    }
  },

  crime: {
    primary: {
      crimeData: []
    },
    secondary: {
      crimeData: []
    }
  },

  transport: {
    primary: {
      transportData: []
    },
    secondary: {
      transportData: []
    }
  },


  
};
