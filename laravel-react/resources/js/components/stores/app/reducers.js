import { initialState } from "./states";

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_LOADED": {
        state.loaded = true;
      return Object.assign({}, state, {
        loaded: state.loaded
      });
    }
    case "UNSET_LOADED": {
      state.loaded = false;
      return Object.assign({}, state, {
        loaded: state.loaded
      });
    }
    case "SET_INVALID": {
      state.invalid = true;
    return Object.assign({}, state, {
      invalid: state.invalid
    });
  }
  case "UNSET_INVALID": {
    state.invalid = false;
    return Object.assign({}, state, {
      invalid: state.invalid
    });
  }
    case "SET_USER_DATA": {
      if(action.data){
        state.user.isLoggedIn = true;
        state.user.userData = action.data;
      }else{
        state.user.isLoggedIn = false;
        state.user.userData = {};
      }
      return Object.assign({}, state, {
        user: state.user
      });
    }
    case "SET_FAVOURITES": {
      state.user.favourites = action.data;
      return Object.assign({}, state, {
        user: state.user
      });
    }
    case "SET_MAP_DATA": {
      if(state.map.primary.mapData.length == 0){
        state.map.primary.mapData = action.data
      }else{
        state.map.secondary.mapData = action.data
      }
      return Object.assign({}, state, {
        map: state.map
      });
    }
    case "REMOVE_PRIMARY_MAP_DATA": {
      state.map.primary.mapData = state.map.secondary.mapData
      state.map.secondary.mapData = []
      return Object.assign({}, state, {
        map: state.map
      });
    }

    case "REMOVE_SECONDARY_MAP_DATA": {
      state.map.secondary.mapData = []
      return Object.assign({}, state, {
        map: state.map
      });
    }

    case "SET_POSTCODE": {
      state.map.postcode = action.data
      return Object.assign({}, state, {
        map: state.map
      });
    }

    case "SET_POSTCODES": {
      
      state.map.postcodes = action.data
      return Object.assign({}, state, {
        map: state.map
      });
    }

    case "REMOVE_POSTCODE": {
      // console.log(state.map.postcodes)
      // console.log(action.data)
      const postcodes = state.map.postcodes.filter(x => x != action.data);
      // console.log(postcodes)
      state.map.postcodes = postcodes
      return Object.assign({}, state, {
        map: state.map
      });
    }

///////////////
    case "SET_SCHOOL_DATA": {
      if(state.school.primary.schoolData.length == 0){
        state.school.primary.schoolData = action.data
      }else{
        state.school.secondary.schoolData = action.data
      }
      return Object.assign({}, state, {
        school: state.school
      });
    }
    case "REMOVE_PRIMARY_SCHOOL_DATA": {
      state.school.primary.schoolData = state.school.secondary.schoolData
      state.school.secondary.schoolData = []
      return Object.assign({}, state, {
        school: state.school
      });
    }
    case "REMOVE_SECONDARY_SCHOOL_DATA": {
      state.school.secondary.schoolData = []
      return Object.assign({}, state, {
        school: state.school
      });
    }

///////////////
    case "SET_WEATHER_NOW": {
      if(Object.keys(state.weather.primary.currentWeather).length == 0){
        // console.log("SETTING PRIMARY WEATHER")
        state.weather.primary.currentWeather = action.data
      }else{
        state.weather.secondary.currentWeather = action.data
      }
      return Object.assign({}, state, {
        weather: state.weather
      });
    }

    case "SET_WEATHER_AVERAGE": {
      if(state.weather.primary.averageWeather.length == 0){
        state.weather.primary.averageWeather = action.data
      }else{
        state.weather.secondary.averageWeather = action.data
      }
      return Object.assign({}, state, {
        weather: state.weather
      });
    }

    case "SET_AIR_QUALITY": {
      if(state.weather.primary.airquality.length == 0){
        state.weather.primary.airquality = action.data
      }else{
        state.weather.secondary.airquality = action.data
      }
      return Object.assign({}, state, {
        weather: state.weather
      });
    }

    case "REMOVE_PRIMARY_WEATHER_DATA": {
      state.weather.primary.currentWeather = state.weather.secondary.currentWeather
      state.weather.primary.averageWeather = state.weather.secondary.averageWeather
      state.weather.primary.airquality = state.weather.secondary.airquality
      state.weather.secondary.currentWeather = {}
      state.weather.secondary.averageWeather = []
      state.weather.secondary.airquality = []
      return Object.assign({}, state, {
        weather: state.weather
      });
    }
    case "REMOVE_SECONDARY_WEATHER_DATA": {
      state.weather.secondary.currentWeather = {}
      state.weather.secondary.averageWeather = []
      state.weather.secondary.airquality = []
      return Object.assign({}, state, {
        weather: state.weather
      });
    }

///////////////
    case "SET_GENERAL_DATA": {
      if(state.general.primary.generalData.length == 0){
        state.general.primary.generalData = action.data
      }else{
        state.general.secondary.generalData = action.data
      }
      return Object.assign({}, state, {
        general: state.general
      });
    }
    case "REMOVE_PRIMARY_GENERAL_DATA": {
      state.general.primary.generalData = state.general.secondary.generalData
      state.general.secondary.generalData = []
      return Object.assign({}, state, {
        general: state.general
      });
    }
    case "REMOVE_SECONDARY_GENERAL_DATA": {
      state.general.secondary.generalData = []
      return Object.assign({}, state, {
        general: state.general
      });
    }

    ///////////////
    case "SET_CRIME_DATA": {
      // console.log('SET_CRIME_DATA')
      
      if(state.crime.primary.crimeData.length == 0){
        // console.log(action.data)
        state.crime.primary.crimeData = action.data
      }else{
        state.crime.secondary.crimeData = action.data
      }
      return Object.assign({}, state, {
        crime: state.crime
      });
    }
    case "REMOVE_PRIMARY_CRIME_DATA": {
      state.crime.primary.crimeData = state.crime.secondary.crimeData
      state.crime.secondary.crimeData = []
      return Object.assign({}, state, {
        crime: state.crime
      });
    }
    case "REMOVE_SECONDARY_CRIME_DATA": {
      state.crime.secondary.crimeData = []
      return Object.assign({}, state, {
        crime: state.crime
      });
    }


    ///////////////
    case "SET_TRANSPORT_DATA": {
      // console.log('SET_TRANSPORT_DATA')
      
      if(state.transport.primary.transportData.length == 0){
        // console.log(action.data)
        state.transport.primary.transportData = action.data
      }else{
        state.transport.secondary.transportData = action.data
      }
      return Object.assign({}, state, {
        transport: state.transport
      });
    }
    case "REMOVE_PRIMARY_TRANSPORT_DATA": {
      state.transport.primary.transportData = state.transport.secondary.transportData
      state.transport.secondary.transportData = []
      return Object.assign({}, state, {
        transport: state.transport
      });
    }
    case "REMOVE_SECONDARY_TRANSPORT_DATA": {
      state.transport.secondary.transportData = []
      return Object.assign({}, state, {
        transport: state.transport
      });
    }
      
    default:
      return state;
  }
};
