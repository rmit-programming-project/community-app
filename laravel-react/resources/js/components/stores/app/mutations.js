const setLoaded = data => ({
  type: "SET_LOADED",
  data
})

const unsetLoaded = data => ({
  type: "UNSET_LOADED",
  data
})

const setInvalid = data => ({
  type: "SET_INVALID",
  data
})

const unsetInvalid = data => ({
  type: "UNSET_INVALID",
  data
})

const setUserData = data => ({
  type: "SET_USER_DATA",
  data
})

const setFavourites = data => ({
  type: "SET_FAVOURITES",
  data
})

const setMapData = data => ({
  type: "SET_MAP_DATA",
  data
});

const removePrimaryMapData = data => ({
  type: "REMOVE_PRIMARY_MAP_DATA",
  data
})

const removeSecondaryMapData = data => ({
  type: "REMOVE_SECONDARY_MAP_DATA",
  data
})


// const setMapData = data => ({
//   type: "SET_MAP_DATA",
//   data
// });

const setPostcode = data => ({
  type: "SET_POSTCODE",
  data
});

const setPostcodes = data =>({
  type: "SET_POSTCODES",
  data
})

const removePostcode = data =>({
  type: "REMOVE_POSTCODE",
  data
})

const setSchoolDataPrimary = data => ({
  type: "SET_SCHOOL_DATA",
  data
});

const removePrimarySchoolData = data => ({
  type: "REMOVE_PRIMARY_SCHOOL_DATA",
  data
})

const removeSecondarySchoolData = data => ({
  type: "REMOVE_SECONDARY_SCHOOL_DATA",
  data
})

const setWeatherNow = data => ({
  type: "SET_WEATHER_NOW",
  data
});

const setWeatherAverage = data => ({
  type: "SET_WEATHER_AVERAGE",
  data
})

const setAirQuality = data => ({
  type: "SET_AIR_QUALITY",
  data
}) 
const removePrimaryWeatherData = data => ({
  type: "REMOVE_PRIMARY_WEATHER_DATA",
  data
})

const removeSecondaryWeatherData = data => ({
  type: "REMOVE_SECONDARY_WEATHER_DATA",
  data
})


const setGeneralDataPrimary = data => ({
  type: "SET_GENERAL_DATA",
  data
});

const removePrimaryGeneralData = data => ({
  type: "REMOVE_PRIMARY_GENERAL_DATA",
  data
})

const removeSecondaryGeneralData = data => ({
  type: "REMOVE_SECONDARY_GENERAL_DATA",
  data
})

const setCrimeDataPrimary = data => ({
  type: "SET_CRIME_DATA",
  data
});

const removePrimaryCrimeData = data => ({
  type: "REMOVE_PRIMARY_CRIME_DATA",
  data
})

const removeSecondaryCrimeData = data => ({
  type: "REMOVE_SECONDARY_CRIME_DATA",
  data
})



const setTransportDataPrimary = data => ({
  type: "SET_TRANSPORT_DATA",
  data
});

const removePrimaryTransportData = data => ({
  type: "REMOVE_PRIMARY_TRANSPORT_DATA",
  data
})

const removeSecondaryTransportData = data => ({
  type: "REMOVE_SECONDARY_TRANSPORT_DATA",
  data
})






  

  export const mutation = {
    setMapData,
    removePrimaryMapData,
    removeSecondaryMapData,
    setPostcode,
    setPostcodes,
    removePostcode,
    setSchoolDataPrimary,
    removePrimarySchoolData,
    removeSecondarySchoolData,
    setWeatherNow,
    setWeatherAverage,
    removePrimaryWeatherData,
    removeSecondaryWeatherData,
    setGeneralDataPrimary,
    removePrimaryGeneralData,
    removeSecondaryGeneralData,
    setUserData,
    setAirQuality,
    setFavourites,
    setCrimeDataPrimary,
    removePrimaryCrimeData,
    removeSecondaryCrimeData,
    setTransportDataPrimary,
    removePrimaryTransportData,
    removeSecondaryTransportData,
    setLoaded,
    unsetLoaded,
    setInvalid,
    unsetInvalid


  };