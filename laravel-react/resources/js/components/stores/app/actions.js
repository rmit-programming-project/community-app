import { mutation } from "./mutations";
import {API} from "../../services/api";


//#########################---USER FEATURES---############################################

export const login = (email, password) => async dispatch => {
  // console.log("inside lgin action")
  
    API.login(email, password).then((resp)=>{
      // console.log(resp)
      if(resp.data){
        // console.log("USER Successfully logged in")
        dispatch(setUserData(resp.data))
      }else{
        // console.log("USER Failed to login", props.usr)
      }
      
    })
  
};

export const registerUser = (name, email, psw) => async dispatch=> {
  // console.log("Registering User")
  API.registerUser(name, email, psw).then((resp)=>{
    if(resp.data){
      // console.log("USER Successfully logged in")
      dispatch(setUserData(resp.data))
    }else{
      // console.log("USER Failed to login", props.usr)
    }
  })
}

export const getFavourites = (userId) => async dispatch => {
  // console.log("Getting favourites")
  API.getFavourites(userId).then((resp)=>{
    dispatch(mutation.setFavourites(resp))
  })
}

export const addFavourite = (suburbId, userId, currentFaves, postcode) => async dispatch => {
  let faveOldList = currentFaves
  let faves  = [];

  if(faveOldList.filter((fave)=>fave.id == suburbId).length > 0){
    // this means that we are removing fave
    // console.log("temp removing fave")
    faves = faveOldList.filter((fave)=> fave.id != suburbId)
  }else{
    // console.log("temp adding fave")
    faveOldList.push({postcode: postcode, id: suburbId})
    faves = faveOldList;
  }
  
  dispatch(mutation.setFavourites(faves))
  API.addFavourite(suburbId).then(()=>{
    dispatch(getFavourites(userId))
  }).catch((err)=>{
    console.error("failed to save favourite")
  })
}


export const setUserData = (usrData) => async dispatch => {
  // console.log("setting user data as = ", usrData)
  dispatch(mutation.setUserData(usrData))
}


export const logout = () => async dispatch => {
  // console.log("inside logout action")
  API.logout().then((resp)=>{
    // console.log("Logged out")
    const propsContainer = document.getElementById("main");
    const props = Object.assign({}, propsContainer.dataset);
    dispatch(setUserData(null))
  })
  
};


//#########################---UPDATE MAP---############################################

export const updateMap = (state) => dispatch => {
  // console.log("updating Map ",state);
  dispatch(setPostcode(''))
  dispatch(mutation.unsetLoaded())
  dispatch(mutation.unsetInvalid())
  API.updateMapLocation(state).then(async(resp)=>{
    
    // console.log(resp)
      // console.log(resp)
    
    dispatch(mutation.setMapData(resp.response))
    // console.log(resp.response[0].latitude)
    // console.log(resp.response[0].longitude)

    dispatch(getGeneralByPostcode(resp.response[0].postcode))  
    dispatch(getAirQuality(resp.response[0].postcode)) 
    dispatch(getSchoolsByPostcode(resp.response[0].postcode))
    dispatch(getCrimeByPostcode(resp.response[0].postcode))
    dispatch(getTransportByPostcode(resp.response[0].postcode))

    dispatch(getWeatherNow(resp.response[0].latitude, resp.response[0].longitude))
    await dispatch(getWeatherAverage(resp.response[0].latitude, resp.response[0].longitude))
    
  }).catch((error)=>{
    dispatch(mutation.setInvalid())
    dispatch(removePostcode(state))
    dispatch(mutation.setLoaded())
    // console.log("implement code to remove invalid here")
  })

  
  
};

export const setPostcodes = (state) => async dispatch => {
  // console.log("updating postcode list")
  dispatch(mutation.setPostcodes(state))
}

export const setPostcode = (state) => async dispatch => {
  // console.log("setting postcode ",state);
  dispatch(mutation.setPostcode(state))
  
};

export const removePostcode = (state) => async dispatch => {
  // console.log("setting postcode ",state);
  dispatch(mutation.removePostcode(state))
  
};

export const removePrimaryMapData = (state) => async dispatch => {
  dispatch(mutation.removePrimaryMapData(state))
}
export const removeSecondaryMapData = (state) => async dispatch => {
  dispatch(mutation.removeSecondaryMapData(state))
}


//#########################---SCHOOL---############################################

export const getSchoolsByPostcode = (postcode) => async dispatch => {
  // console.log("updating schools Now");

  API.getSchoolDataByPostcode(postcode).then((resp)=>{
    // console.log(resp)
    dispatch(mutation.setSchoolDataPrimary(resp))
  })
  
};

export const removePrimarySchoolData = (state) => async dispatch => {
  dispatch(mutation.removePrimarySchoolData(state))
}
export const removeSecondarySchoolData = (state) => async dispatch => {
  dispatch(mutation.removeSecondarySchoolData(state))
}


//#########################---WEATHER---############################################

export const getWeatherNow = (lat, long) => async dispatch => {
  // console.log("updating Weather Now");
  
  API.getWeatherNow(lat, long).then((resp)=>{
    dispatch(mutation.setWeatherNow(resp))
  })
};

export const getWeatherAverage = (lat, long) => async dispatch => {
  // console.log("setting average weather ");
  API.getMonthlyWeatherAverage(lat, long).then((resp)=>{
    dispatch(mutation.setWeatherAverage(resp))
    dispatch(mutation.setLoaded())
  })
};

export const getAirQuality = (postcode) => async dispatch => {
  // console.log("setting set AirQuality");
  API.getAirQuality(postcode).then((resp)=>{
    dispatch(mutation.setAirQuality(resp))
  })
};

export const removePrimaryWeatherData = (state) => async dispatch => {
  dispatch(mutation.removePrimaryWeatherData(state))
}
export const removeSecondaryWeatherData = (state) => async dispatch => {
  dispatch(mutation.removeSecondaryWeatherData(state))
}

//#########################---GENERAL---############################################

export const getGeneralByPostcode = (postcode) => async dispatch => {
  // console.log(`updating general Now with ${postcode}`);
  API.getGeneralDataByPostcode(postcode).then((resp)=>{
    // console.log(resp)
    dispatch(mutation.setGeneralDataPrimary(resp))
  })
  
};

export const removePrimaryGeneralData = (state) => async dispatch => {
  dispatch(mutation.removePrimaryGeneralData(state))
}
export const removeSecondaryGeneralData = (state) => async dispatch => {
  dispatch(mutation.removeSecondaryGeneralData(state))
}

//#########################---CRIME---############################################

export const getCrimeByPostcode = (postcode) => async dispatch => {
  // console.log(`updating general Now with ${postcode}`);
  API.getCrimeData(postcode).then((resp)=>{
    // console.log(resp)
    dispatch(mutation.setCrimeDataPrimary(resp))
  })
  
};

export const removePrimaryCrimeData = (state) => async dispatch => {
  dispatch(mutation.removePrimaryCrimeData(state))
}
export const removeSecondaryCrimeData = (state) => async dispatch => {
  dispatch(mutation.removeSecondaryCrimeData(state))
}


//#########################---Transport---############################################

export const getTransportByPostcode = (postcode) => async dispatch => {
  // console.log(`updating trasnport Now with ${postcode}`);
  API.getTransportData(postcode).then((resp)=>{
    // console.log(resp)
    dispatch(mutation.setTransportDataPrimary(resp))
  })
  
};

export const removePrimaryTransportData = (state) => async dispatch => {
  dispatch(mutation.removePrimaryTransportData(state))
}
export const removeSecondaryTransportData = (state) => async dispatch => {
  dispatch(mutation.removeSecondaryTransportData(state))
}

// //#########################---LOADED---############################################
// export const setLoaded = () => dispatch => {
//     dispatch(mutation.setLoaded())
// }

// export const unsetLoaded = () => dispatch => {
//   dispatch(mutation.unsetLoaded())
// }

