<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::get('/details', 'SuburbDetailsController@index');

Route::get('/suburb/{suburb}', 'ApidataController@suburbInfo');
Route::get('/suburb/postcode/{suburb}', 'ApidataController@suburbInfoPostcode');

Route::get('/realesate-info/{suburb}', 'ApidataController@getRealesateInfo');
Route::get('/fetchData', 'DatafetcherController@index');


Route::get('/schools/postcode/{postcode}', 'SchoolInfoController@index');
Route::get('/schools/suburb/{suburb}', 'SchoolInfoController@Suburb');
Route::get('/schools/postcode/historic/{postcode}', 'SchoolInfoController@HistoricPostcode');
Route::get('/schools/suburb/historic/{suburb}', 'SchoolInfoController@HistoricSuburb');
Route::get('/schools/{postcode}/{suburb}', 'SchoolInfoController@PostcodeSuburb');


Route::get('/details', 'SuburbDetailsController@index');

Route::post('/register', 'API\UserController@store');
//Route::post('/login', 'API\AuthController@login');
Route::post('/user/login', 'API\UserController@login');
Route::post('/logout', 'API\UserController@logout');



Route::get('suburb-data', 'Api\WeatherApiController@getSuburbData');
Route::get('monthly-data', 'Api\WeatherApiController@getMonthlyData');
Route::get('air-quality', 'Api\WeatherApiController@getAirQuality');

Route::post('/add-suburb-to-favourites/{suburb}', 'SuburbController@addSuburbToFavourites');
Route::get('/favorite-suburbs/{userId}', 'SuburbController@getFavouriteSuburbs');

Route::post('/download', 'DownloadController@index');


Route::get('/transport/{postcode}', 'TransportController@index');
Route::get('/crime/{postcode}', 'CrimeDetailsController@index');
