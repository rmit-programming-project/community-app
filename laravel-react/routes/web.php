<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/login', function () {
//     return view('welcome');
// });
Route::view('/{path?}', 'welcome');
Auth::routes();

// Route::get('/', function () {
//     return view('welcome');
   
// });


// Route::get('/compareSuburb/suburb1/{postcode1}/suburb2/{postcode2}', 'SuburbController@compare');


// Route::get('/scrapeSuburbs', 'DataScraperController@suburbsInfo');


// Route::get('/home', 'HomeController@index')->name('home');



// Route::get('/home', 'HomeController@index')->name('home');
Route::post('/add-suburb-to-favourites/{suburb}', 'SuburbController@addSuburbToFavourites');
Route::get('/favorite-suburbs/{userId}', 'SuburbController@getFavouriteSuburbs');
