#!/bin/bash
php artisan migrate
composer dumpautoload
php artisan db:seed --class=CrimeNSWSeeder & \
php artisan db:seed --class=CrimeVICSeeder & \
php artisan db:seed --class=CrimeACTSeeder & \
php artisan db:seed --class=PostcodeSeeder & \
php artisan db:seed --class=SchoolLocationSeeder & \
php artisan db:seed --class=SchoolProfileSeeder & \
php artisan db:seed --class=SuburbSeeder & \ 
php artisan db:seed --class=TransportStopSeeder & \ 
jobs
