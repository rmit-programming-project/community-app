

 DROP TABLE IF EXISTS users; 
 CREATE TABLE IF NOT EXISTS users  
 ( 
 userid  VARCHAR(100)   
, firstname  VARCHAR(20)   
, lastname  VARCHAR(20)     
, email  VARCHAR(50)    
    , PRIMARY KEY (userid) 
 );


 DROP TABLE IF EXISTS locations; 
 CREATE TABLE IF NOT EXISTS locations  
 ( 
 postcode  VARCHAR(100)   
, state  VARCHAR(20)   
, city  VARCHAR(20)     
, email  VARCHAR(50)  
, longitude INT(8)
, latitude INT(8)
    , PRIMARY KEY (postcode) 
 );