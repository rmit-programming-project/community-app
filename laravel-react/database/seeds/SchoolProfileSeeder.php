<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class SchoolProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Log::debug('Seeding into school_profiles');
        $csvFile = public_path().'/../data/school-profile-2008-2019.csv';
        Log::debug($csvFile);
        if (($file = fopen($csvFile, 'r')) !== FALSE)
            {
                
                Log::debug($file);
                while (($row = fgetcsv($file, 1000, ',')) !== FALSE)
                {
                    DB::table('school_profiles')->insert([
                        'calendar-year'=> $row[0],
                        'acara-sml-id'=> $row[1],
                        'age-id'=> (int)$row[2],
                        'school-name'=> $row[3],
                        'suburb'=> $row[4],
                        'state'=> $row[5],
                        'postcode'=> $row[6],
                        'school-sector'=> $row[7],
                        'school-type'=> $row[8],
                        'campus-type'=> $row[9],
                        'rolled-reporting-description'=> $row[10],
                        'school-url'=> $row[11],
                        'governing-body'=> $row[12],
                        'governing-body-url'=> $row[13],
                        'year-range'=> $row[14],
                        'geolocation'=> $row[15],
                        'icsea'=> (int)$row[16],
                        'icsea-percentile'=> (int)$row[17],
                        'bottom-sea-quarter'=> (int)$row[18],
                        'lower-middle-sea-quarter'=> (int)$row[19],
                        'upper-middle-sea-quarter'=> (int)$row[20],
                        'top-sea-quarter'=> (int)$row[21],
                        'teaching-staff'=> (int)$row[22],
                        'full-time-teaching-staff'=> (int)$row[23],
                        'non-teaching-staff'=> (int)$row[24],
                        'full-time-non-teaching-staff'=> (int)$row[25],
                        'total-enrolments'=> (int)$row[26],
                        'girls-enrolments'=> (int)$row[27],
                        'boys-enrolments'=> (int)$row[28],
                        'full-time-enrolments'=> (int)$row[29],
                        'indigenous-enrolments'=> (int)$row[30],
                        'non-english-background'=> (int)$row[31],
                    ]);

                }
                fclose($file);
                
            }
        Log::debug('school_profiles seeding completed');
    }
}


