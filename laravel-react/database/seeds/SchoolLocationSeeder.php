<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use function Psy\debug;

class SchoolLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // https://laracasts.com/discuss/channels/general-discussion/laravel-5-csv-seeding

    public function run()
    {
        Log::debug('Seeding into school-locations');
        $csvFile = public_path().'/../data/school-locations.csv';
        Log::debug($csvFile);
        if (($file = fopen($csvFile, 'r')) !== FALSE)
            {
                
                Log::debug($file);
                while (($row = fgetcsv($file, 1000, ',')) !== FALSE)
                {
                    DB::table('school_locations')->insert([
                        'calendar_year' => $row[0],
                        'acara_id' => $row[1],
                        'age_id' => (int)$row[2],
                        'school_name' => $row[3],
                        'suburb' => $row[4],
                        'state' => $row[5],
                        'postcode' => $row[6],
                        'school_sector' => $row[7],
                        'school_type' => $row[8],
                        'campus_type' => $row[9],
                        'rolled_reporting_description' => $row[10],
                        'latitude' => $row[11],
                        'longitude' => $row[12],
                        'statistical_area_1' => $row[13],
                        'statistical_area_2' => $row[14],
                        'name_of_statistical_area_2' => $row[15],
                        'statistical_area_3' => $row[16],
                        'name_of_statistical_area_3' => $row[17],
                        'statistical_area_4' => $row[18],
                        'name_of_statistical_area_4' => $row[19],
                        'abs_remoteness_area' => $row[20],
                    ]);

                }
                fclose($file);
                
            }
        Log::debug('school-locations seeding completed');

        
    }
}



            