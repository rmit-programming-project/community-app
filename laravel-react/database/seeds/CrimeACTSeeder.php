<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class CrimeACTSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Log::debug('Seeding into ACTCrimestats');
        $csvFile = public_path().'/../data/ACTCrimeStats.csv';
        Log::debug($csvFile);
        if (($file = fopen($csvFile, 'r')) !== FALSE)
            {
                
                Log::debug($file);
                while (($row = fgetcsv($file, 1000, ',')) !== FALSE)
                {
                    DB::table('ACT_CrimeStats')->insert([
                        'suburb'=> $row[0],
                        'calendar-year'=> $row[1],
                        'crime-type'=> $row[2],
                        'amount_crimes'=> $row[3],
                        ]);

                }
                fclose($file);
                
            }
        Log::debug('ACTCrimestats seeding completed');
    }
}
