<?php

use Illuminate\Database\Seeder;

class CrimeNSWSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Log::debug('Seeding into NSWCrimestats');
        $csvFile = public_path().'/../data/NSWCrimeStats.csv';
        Log::debug($csvFile);
        if (($file = fopen($csvFile, 'r')) !== FALSE)
            {
                
                Log::debug($file);
                while (($row = fgetcsv($file, 1000, ',')) !== FALSE)
                {
                    DB::table('NSW_CrimeStats')->insert([
                        'postcode'=>$row[0],
                        'calendar-year'=>$row[1],
                        'crime-type'=>$row[2],
                        'crime-type2'=>$row[3],
                        'amount_crimes'=>$row[4],

                    ]);
                }
                fclose($file);
                
            }
        Log::debug('NSW_Crimestats seeding completed');
    }
}
