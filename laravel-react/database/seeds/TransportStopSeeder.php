<?php

use Illuminate\Database\Seeder;

class TransportStopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Log::debug('transport_stops seeding started');
        $csvPath = public_path().'/../data/stops/';
        Log::debug($csvPath);
        $files = scandir(public_path() . '/../data/stops/');

        foreach($files as $file){
            
            if ($file == '.' || $file =='..'){continue;}

            $csv = $this->BuildData($csvPath,$file);

            
            array_walk($csv, function($row) {
                // Log::debug("inserting to DB");
                
                DB::table('transport_stops')->insert([
                    'stop_id' => $row['stop_id'],
                    'stop_name' => $row['stop_name'],
                    'stop_lat' => $row['stop_lat'],
                    'stop_lon' => $row['stop_lon'],
                ]);
            });

   
        }
        

        Log::debug('transport_stops seeding completed');
    }

    private function BuildData($csvPath,$file){
        Log::debug("building new file");
        Log::debug($csvPath.$file);

        $csv = array();
            $i = 0;
            if (($handle = fopen($csvPath.$file, "r")) !== false) {
                $columns = fgetcsv($handle, 1000, ",");
                // Log::debug($columns);
                while (($row = fgetcsv($handle, 1000, ",")) !== false) {
                    $csv[$i] = array_combine($columns, $row);
                    $i++;
                }
                fclose($handle);
            }
        return $csv;

    }
}
