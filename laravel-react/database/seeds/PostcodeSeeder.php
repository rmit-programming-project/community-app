<?php

use Illuminate\Database\Seeder;

class PostcodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Log::debug('Seeding into postcodes');
        $csvFile = public_path().'/../data/postcodes.csv';
        Log::debug($csvFile);
        if (($file = fopen($csvFile, 'r')) !== FALSE)
            {
                
                Log::debug($file);
                while (($row = fgetcsv($file, 1000, ',')) !== FALSE)
                {
                    DB::table('postcodes')->insert([
                        'postcode' => $row[0],
                        'suburb' => $row[1],
                        'state_code' => $row[3],
                        'longtitude' => $row[4],
                        'latitude' => $row[5],
                    ]);

                }
                fclose($file);
                
            }
        Log::debug('postcodes seeding completed');

        
    }
}
