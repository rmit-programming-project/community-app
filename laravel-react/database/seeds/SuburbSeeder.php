<?php

use Illuminate\Database\Seeder;
use App\Suburb;
use Illuminate\Support\Facades\Log;

class SuburbSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $suburbs = Suburb::all();
   
        foreach ($suburbs as $suburb) {

            
            $medianPrice = rand(200000.1,2000000.1);
            $bed3 = $medianPrice + rand(-50000,50000);
            $bed2 = $bed3 - rand(30000,100000);
            $bed4 = $bed3 + rand(50000,250000);


            $suburb['2_bedroom_price_avg'] = $bed2;
            $suburb['3_bedroom_price_avg'] = $bed3;
            $suburb['4_bedroom_price_avg'] = $bed4;
            $suburb['median_house_price'] = $medianPrice;
            $suburb['price_movement_five_years'] = rand(-15,30);
            $suburb->update();
            }
    }
        
    

}
