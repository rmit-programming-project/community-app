<?php

use Illuminate\Database\Seeder;

class CrimeVICSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Log::debug('Seeding into crime-stats-vic');
        $csvFile = public_path().'/../data/VICCrimeStats.csv';
        Log::debug($csvFile);
        if (($file = fopen($csvFile, 'r')) !== FALSE)
            {
                
                Log::debug($file);
                while (($row = fgetcsv($file, 1000, ',')) !== FALSE)
                {
                    DB::table('VIC_CrimeStats')->insert([

                        'year'=>$row[0],
                        'postcode'=>$row[1],
                        'suburb'=>$row[2],
                        'crime-type'=>$row[3],
                        'amount_crimes'=>$row[4],

                    ]);
                }
                fclose($file);
                
            }
        Log::debug('crime-stats-vic seeding completed');
    }
}
