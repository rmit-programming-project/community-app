<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransportStops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transport_stops', function (Blueprint $table) {
            $table->string('stop_id')->nullable();
            $table->string('stop_name')->nullable();
            $table->string('stop_lat')->nullable();
            $table->string('stop_lon')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_stops');
    }
}
