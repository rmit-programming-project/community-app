<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_profiles', function (Blueprint $table) {
            $table->string('calendar-year')->nullable();
            $table->unsignedBigInteger('acara-sml-id')->nullable();
            $table->integer('age-id')->nullable();
            $table->string('school-name')->nullable();
            $table->string('suburb')->nullable();
            $table->string('state')->nullable();
            $table->smallInteger('postcode')->nullable();
            $table->string('school-sector')->nullable();
            $table->string('school-type')->nullable();
            $table->string('campus-type')->nullable();
            $table->string('rolled-reporting-description')->nullable();
            $table->string('school-url')->nullable();
            $table->string('governing-body')->nullable();
            $table->string('governing-body-url')->nullable();
            $table->string('year-range')->nullable();
            $table->string('geolocation')->nullable();
            $table->integer('icsea')->nullable();
            $table->integer('icsea-percentile')->nullable();
            $table->integer('bottom-sea-quarter')->nullable();
            $table->integer('lower-middle-sea-quarter')->nullable();
            $table->integer('upper-middle-sea-quarter')->nullable();
            $table->integer('top-sea-quarter')->nullable();
            $table->integer('teaching-staff')->nullable();
            $table->integer('full-time-teaching-staff')->nullable();
            $table->integer('non-teaching-staff')->nullable();
            $table->integer('full-time-non-teaching-staff')->nullable();
            $table->integer('total-enrolments')->nullable();
            $table->integer('girls-enrolments')->nullable();
            $table->integer('boys-enrolments')->nullable();
            $table->integer('full-time-enrolments')->nullable();
            $table->integer('indigenous-enrolments')->nullable();
            $table->integer('non-english-background')->nullable();
        });
    }

    
    
    
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_profiles');
    }
}
