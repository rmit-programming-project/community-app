<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VICCrimeStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('VIC_CrimeStats', function (Blueprint $table) {
            $table->string('year');
            $table->smallInteger('postcode');
            $table->string('suburb');
            $table->string('crime-type');
            $table->Integer('amount_crimes');
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
