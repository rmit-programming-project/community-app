<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    // data downloaded from myschools.gov.au
    public function up()
    {
        Schema::create('school_locations', function (Blueprint $table) {
            $table->string('calendar_year')->nullable();
            $table->unsignedBigInteger('acara_id')->nullable();
            $table->integer('age_id')->nullable();
            $table->string('school_name')->nullable();
            $table->string('suburb')->nullable();
            $table->string('state')->nullable();
            $table->smallInteger('postcode')->nullable();
            $table->string('school_sector')->nullable();
            $table->string('school_type')->nullable();
            $table->string('campus_type')->nullable();
            $table->string('rolled_reporting_description')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->bigInteger('statistical_area_1')->nullable();
            $table->integer('statistical_area_2')->nullable();
            $table->string('name_of_statistical_area_2')->nullable();
            $table->integer('statistical_area_3')->nullable();
            $table->string('name_of_statistical_area_3')->nullable();
            $table->integer('statistical_area_4')->nullable();
            $table->string('name_of_statistical_area_4')->nullable();
            $table->string('abs_remoteness_area')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_locations');
    }
}
