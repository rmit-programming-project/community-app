<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NSWCrimeStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('NSW_CrimeStats', function (Blueprint $table) {
            $table->smallinteger('postcode');
            $table->smallinteger('calendar-year');
            $table->string('crime-type');
            $table->string('crime-type2');
            $table->Integer('amount_crimes');
        });        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
