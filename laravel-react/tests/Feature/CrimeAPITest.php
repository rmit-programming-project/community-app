<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;

class CrimeAPITest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCrime1()
    {
        $postcode = 2640;
        Log::debug("Testing: http://hostname/api/crime/$postcode");
        $response = $this->json('GET', "/api/crime/$postcode");
 
        $response->assertStatus(200)->assertJsonFragment(
            [
                "totalCrimes" => 5145,
                "totalMurders" => 1,
                "totalAssault" => 478,
                "totalBurgalary" => 16,
                "totalTheft" => 220,
            ]);
    
    }


    public function testCrime2()
    {
        $postcode = 2077;
        Log::debug("Testing: http://hostname/api/crime/$postcode");
        $response = $this->json('GET', "/api/crime/$postcode");
 
        $response->assertStatus(200)->assertJsonFragment(
            [
                "totalCrimes" => 4338,
                "totalMurders" => 0,
                "totalAssault" => 176,
                "totalBurgalary" => 6,
                "totalTheft" => 132,
            ]);
    
    }


    public function testCrime3()
    {
        $postcode = 2905;
        Log::debug("Testing: http://hostname/api/crime/$postcode");
        $response = $this->json('GET', "/api/crime/$postcode");
 
        $response->assertStatus(200)->assertJsonFragment(
            [
                "totalCrimes" => 1772,
                "totalMurders" => 1,
                "totalAssault" => 170,
                "totalBurgalary" => 130,
                "totalTheft" => 104,
            ]);
    
    }


    public function testCrime4()
    {
        $postcode = 3678;
        Log::debug("Testing: http://hostname/api/crime/$postcode");
        $response = $this->json('GET', "/api/crime/$postcode");
 
        $response->assertStatus(200)->assertJsonFragment(
            [
                "totalCrimes" => 161,
                "totalMurders" => "Data Not Available",
                "totalAssault" => 21,
                "totalBurgalary" => 20,
                "totalTheft" => 29,
            ]);
    
    }

    public function testCrime5()
    {
        $postcode = 4371;
        Log::debug("Testing: http://hostname/api/crime/$postcode");
        $response = $this->json('GET', "/api/crime/$postcode");
 
        $response->assertStatus(200)->assertJsonFragment(
            [
                "totalCrimes" => 2,
                "totalMurders" => 0,
                "totalAssault" => 0,
                "totalBurgalary" => 1,
                "totalTheft" => 1,
            ]);
    
    }


    public function testCrime6()
    {
        $postcode = 5260;
        Log::debug("Testing: http://hostname/api/crime/$postcode");
        $response = $this->json('GET', "/api/crime/$postcode");
 
        $response->assertStatus(200)->assertJsonFragment(
            [
                "totalCrimes" => 87,
                "totalMurders" => 0,
                "totalAssault" => 10,
                "totalBurgalary" => 13,
                "totalTheft" => 42,
            ]);
    
    }

    public function testCrime7()
    {
        $postcode = 6056;
        Log::debug("Testing: http://hostname/api/crime/$postcode");
        $response = $this->json('GET', "/api/crime/$postcode");
 
        $response->assertStatus(200)->assertJsonFragment(
            [
                "totalCrimes" => 7426,
                "totalMurders" => 1,
                "totalAssault" => 961,
                "totalBurgalary" => 677,
                "totalTheft" => 2672,
            ]);
    
    }
}
