<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;

class WeatherAPITest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testWeather1()
    {
        $months = ["January","February","March","April","May","June","July", "August","September","October","November","December"];
        $lat = -32.61;
        $lng = 149.57;
        Log::debug("Testing: http://hostname/api/monthly-data?lat=$lat&lng=$lng");
        $response = $this->json('GET', "/api/monthly-data?lat=$lat&lng=$lng");
        $responseJson = json_decode($response->content(), true);
        
        // Log::debug($responseJson);    

        foreach ($months as $month){
            $response->assertStatus(200)->assertJsonFragment(["name" => $month]);
        }
        $avg_temp = array_key_exists('avg_temp',$responseJson[0]);
        $avg_sun = array_key_exists('avg_sun',$responseJson[0]);
        $rain_days = array_key_exists('rain_days',$responseJson[0]);
        $avg_rain = array_key_exists('avg_rain',$responseJson[0]);

        
        $this->assertTrue($avg_temp);
        $this->assertTrue($avg_sun);
        $this->assertTrue($rain_days);
        $this->assertTrue($avg_rain);
 
    
    }

    public function testWeather2()
    {
        $months = ["January","February","March","April","May","June","July", "August","September","October","November","December"];
        $lat = -33.70;
        $lng = 151.10;
        Log::debug("Testing: http://hostname/api/monthly-data?lat=$lat&lng=$lng");
        $response = $this->json('GET', "/api/monthly-data?lat=$lat&lng=$lng");
        $responseJson = json_decode($response->content(), true);
        
        // Log::debug($responseJson);    

        foreach ($months as $month){
            $response->assertStatus(200)->assertJsonFragment(["name" => $month]);
        }
        $avg_temp = array_key_exists('avg_temp',$responseJson[0]);
        $avg_sun = array_key_exists('avg_sun',$responseJson[0]);
        $rain_days = array_key_exists('rain_days',$responseJson[0]);
        $avg_rain = array_key_exists('avg_rain',$responseJson[0]);

        
        $this->assertTrue($avg_temp);
        $this->assertTrue($avg_sun);
        $this->assertTrue($rain_days);
        $this->assertTrue($avg_rain);
 
    
    }

    public function testWeather3()
    {
        $months = ["January","February","March","April","May","June","July", "August","September","October","November","December"];
        $lat = -28.53;
        $lng = 115.92;
        Log::debug("Testing: http://hostname/api/monthly-data?lat=$lat&lng=$lng");
        $response = $this->json('GET', "/api/monthly-data?lat=$lat&lng=$lng");
        $responseJson = json_decode($response->content(), true);
        
        // Log::debug($responseJson);    

        foreach ($months as $month){
            $response->assertStatus(200)->assertJsonFragment(["name" => $month]);
        }
        $avg_temp = array_key_exists('avg_temp',$responseJson[0]);
        $avg_sun = array_key_exists('avg_sun',$responseJson[0]);
        $rain_days = array_key_exists('rain_days',$responseJson[0]);
        $avg_rain = array_key_exists('avg_rain',$responseJson[0]);

        
        $this->assertTrue($avg_temp);
        $this->assertTrue($avg_sun);
        $this->assertTrue($rain_days);
        $this->assertTrue($avg_rain);
 
    
    }

    public function testSuburbData1()
    {
        $lat = -32.61;
        $lng = 149.57;
        Log::debug("Testing: http://hostname/api/suburb-data?lat=$lat&lng=$lng");
        $response = $this->json('GET', "/api/suburb-data?lat=$lat&lng=$lng");
        $responseJson = json_decode($response->content(), true);

        $current = $responseJson['current'];
         
        $temp = array_key_exists('temp',$current);
        $cloud = array_key_exists('clouds',$current);
        $wind = array_key_exists('wind_speed',$current);

        $this->assertTrue($temp);
        $this->assertTrue($cloud);
        $this->assertTrue($wind);
        

    }
    
    public function testSuburbData2()
    {
        $lat = -33.70;
        $lng = 151.10;
        Log::debug("Testing: http://hostname/api/suburb-data?lat=$lat&lng=$lng");
        $response = $this->json('GET', "/api/suburb-data?lat=$lat&lng=$lng");
        $responseJson = json_decode($response->content(), true);

        $current = $responseJson['current'];
         
        $temp = array_key_exists('temp',$current);
        $cloud = array_key_exists('clouds',$current);
        $wind = array_key_exists('wind_speed',$current);

        $this->assertTrue($temp);
        $this->assertTrue($cloud);
        $this->assertTrue($wind);
        

    }

    public function testSuburbData3()
    {
        $lat = -28.53;
        $lng = 115.92;
        Log::debug("Testing: http://hostname/api/suburb-data?lat=$lat&lng=$lng");
        $response = $this->json('GET', "/api/suburb-data?lat=$lat&lng=$lng");
        $responseJson = json_decode($response->content(), true);

        $current = $responseJson['current'];
         
        $temp = array_key_exists('temp',$current);
        $cloud = array_key_exists('clouds',$current);
        $wind = array_key_exists('wind_speed',$current);

        $this->assertTrue($temp);
        $this->assertTrue($cloud);
        $this->assertTrue($wind);
        

    }

    public function testAirQuality1()
    {
        $postcode = 3083;
        Log::debug("Testing: http://hostname/api/air-quality?postcode=$postcode");
        $response = $this->json('GET', "/api/air-quality?postcode=$postcode");
        $responseJson = json_decode($response->content(), true);

        $airquality = array_key_exists('aqi',$responseJson);

        $this->assertTrue($airquality);

    }

    public function testAirQuality2()
    {
        $postcode = 2141;
        Log::debug("Testing: http://hostname/api/air-quality?postcode=$postcode");
        $response = $this->json('GET', "/api/air-quality?postcode=$postcode");
        $responseJson = json_decode($response->content(), true);

        $airquality = array_key_exists('aqi',$responseJson);

        $this->assertTrue($airquality);

    }

    public function testAirQuality3()
    {
        $postcode = 2640;
        Log::debug("Testing: http://hostname/api/air-quality?postcode=$postcode");
        $response = $this->json('GET', "/api/air-quality?postcode=$postcode");
        $responseJson = json_decode($response->content(), true);

        $airquality = array_key_exists('aqi',$responseJson);

        $this->assertTrue($airquality);

    }
}
