<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;

use function PHPSTORM_META\type;

class SchoolsAPITest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testSchoolAPIRunning()
    {
        $postcode = 3000;
        $response = $this->json('GET', "/api/schools/postcode/$postcode");
        $response->assertStatus(200);
    }

    public function testSchoolAPIFailure()
    {
        $postcode = 3000;
        $response = $this->json('GET', "/api/schools/postcode/");
        $response->assertStatus(404);
    }

    public function testSchools1()
    {
        $suburb = "albury";
        $postcode = 2640;
        $response = $this->json('GET', "http://hostname/api/schools/$postcode/$suburb");

        $response->assertStatus(200)->assertJson(
            [[
                [
                    "school_name" => "Indie School Albury",
                    "latitude" => -36.089672,
                    "longitude" => 146.913269,
                    "school_type" => "Special"
                ],
                [[
                    "boys-enrolments" => "48",
                    "girls-enrolments" => "59",
                    "indigenous-enrolments" => "28"
                ]]
            ]]
        );
    }

    public function testSchools2()
    {
        $postcode = 2141;
        $numberSchools = 3;

        $response = $this->json('GET', "http://hostname/api/schools/postcode/$postcode");
        $actualnumberSchool = count(json_decode($response->content()));

        $response->assertStatus(200);
        $this->assertTrue($numberSchools == $actualnumberSchool);
    }

    public function testSchools3()
    {
        $postcode = 2077;
        $suburb = "Hornsby";
        $numberSchools = 6;

        $response = $this->json('GET', "http://hostname/api/schools/$postcode/$suburb");
        $actualnumberSchool = count(json_decode($response->content()));

        $response->assertStatus(200);
        $this->assertTrue($numberSchools == $actualnumberSchool);
    }

    public function testSchools4()
    {
        $suburb = "Mudgee";
        $postcode = 2850;
        $response = $this->json('GET', "http://hostname/api/schools/$postcode/$suburb");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "school_name" => "Mudgee High School",
                "latitude" => -32.597793,
                "longitude" => 149.583627,
                "school_type" => "Secondary"
            ]
        )->assertJsonFragment([
            "girls-enrolments" => 381,
            "boys-enrolments" => 439,
            "indigenous-enrolments" => 12
            ]);
    }


    public function testSchools5()
    {
        $postcode = 4213;
        $suburb = "Mudgeeraba";
        $numberSchools = 5;

        $response = $this->json('GET', "http://hostname/api/schools/$postcode/$suburb");
        $actualnumberSchool = count(json_decode($response->content()));

        $response->assertStatus(200);
        $this->assertTrue($numberSchools == $actualnumberSchool);
    }

    public function testSchools6()
    {
        $suburb = "Oxley";
        $postcode = 3678;
        Log::debug("Testing: http://hostname/api/schools/$postcode/$suburb");
        $response = $this->json('GET', "http://hostname/api/schools/$postcode/$suburb");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "school_name" => "Oxley Primary School",
                "latitude" => -36.441561,
                "longitude" => 146.360637,
                "school_type" => "Primary"
            ]
        )->assertJsonFragment([
            "girls-enrolments" => 54,
            "boys-enrolments" => 50,
            "indigenous-enrolments" => 7
            ]);
    }

    public function testSchools7()
    {
        
        $postcode = 4371;
        Log::debug("Testing: http://hostname/api/schools/postcode/$postcode");
        $response = $this->json('GET', "http://hostname/api/schools/postcode/$postcode");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "school_name" => "Yangan State School",
                "latitude" => -28.198089,
                "longitude" => 152.215517,
                "school_type" => "Primary"
            ]
        );
    }


    public function testSchools8()
    {
        $postcode = 5260;
        $suburb = "Tailem Bend";
        $numberSchools = 1;
        
        Log::debug("Testing: http://hostname/api/schools/$postcode/$suburb");

        $response = $this->json('GET', "http://hostname/api/schools/$postcode/$suburb");
        $actualnumberSchool = count(json_decode($response->content()));

        $response->assertStatus(200);
        $this->assertTrue($numberSchools == $actualnumberSchool);
    }

    
    public function testSchools9()
    {
        $postcode = 5301;
        $numberSchools = 1;

        Log::debug("Testing: http://hostname/api/schools/postcode/$postcode");

        $response = $this->json('GET', "http://hostname/api/schools/postcode/$postcode");
        $actualnumberSchool = count(json_decode($response->content()));

        $response->assertStatus(200);
        $this->assertTrue($numberSchools == $actualnumberSchool);
    }

    public function testSchools10()
    {
        $postcode = "6056";
        $suburb = "Swan View";
        $numberSchools = 2;

        Log::debug("Testing: http://hostname/api/schools/$postcode/$suburb");

        $response = $this->json('GET', "http://hostname/api/schools/$postcode/$suburb");
        $actualnumberSchool = count(json_decode($response->content()));

        $response->assertStatus(200);
        $this->assertTrue($numberSchools == $actualnumberSchool);
    }

    

}
