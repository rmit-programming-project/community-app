<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;

class TransportAPITest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testTransport1()
    {
        $postcode = 2640;
        $count = 2064;
        
        Log::debug("Testing: http://hostname/api/transport/$postcode");
        $response = $this->json('GET', "/api/transport/$postcode");
        // $responseJson = json_decode($response->content(), true);

        $response->assertStatus(200)->assertJsonFragment(["totalCount" => $count]);
    }

    public function testTransport2()
    {
        $postcode = 5260;
        $count = 161;
        
        Log::debug("Testing: http://hostname/api/transport/$postcode");
        $response = $this->json('GET', "/api/transport/$postcode");
        // $responseJson = json_decode($response->content(), true);

        $response->assertStatus(200)->assertJsonFragment(["totalCount" => $count]);
    }

    public function testTransport3()
    {
        $postcode = 4371;
        $count = 6;
        
        Log::debug("Testing: http://hostname/api/transport/$postcode");
        $response = $this->json('GET', "/api/transport/$postcode");
        // $responseJson = json_decode($response->content(), true);

        $response->assertStatus(200)->assertJsonFragment(["totalCount" => $count]);
    }


}
