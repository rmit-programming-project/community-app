<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;

class HousingTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testHousing1()
    {

        $suburb = "kambah";
        Log::debug("Testing: http://hostname/api/suburb/$suburb");
        $response = $this->json('GET', "/api/suburb/$suburb");
        $responseJson = json_decode($response->content(), true);
        $median_house_price = strlen($responseJson[0]['median_house_price']);
        $price_movement_five_years = strlen($responseJson[0]['price_movement_five_years']);
        $bedroom_price_avg_2 = strlen($responseJson[0]['2_bedroom_price_avg']);
        $bedroom_price_avg_3 = strlen($responseJson[0]['3_bedroom_price_avg']);
        $bedroom_price_avg_4 = strlen($responseJson[0]['4_bedroom_price_avg']);
        
        $response->assertStatus(200);
        $this->assertTrue($median_house_price > 0);
        $this->assertTrue($price_movement_five_years > 0);
        $this->assertTrue($bedroom_price_avg_2 > 0);
        $this->assertTrue($bedroom_price_avg_3 > 0);
        $this->assertTrue($bedroom_price_avg_4 > 0);
    
    }

    public function testHousing2()
    {

        $suburb = "orange";
        Log::debug("Testing: http://hostname/api/suburb/$suburb");
        $response = $this->json('GET', "/api/suburb/$suburb");
        $responseJson = json_decode($response->content(), true);
        $median_house_price = strlen($responseJson[0]['median_house_price']);
        $price_movement_five_years = strlen($responseJson[0]['price_movement_five_years']);
        $bedroom_price_avg_2 = strlen($responseJson[0]['2_bedroom_price_avg']);
        $bedroom_price_avg_3 = strlen($responseJson[0]['3_bedroom_price_avg']);
        $bedroom_price_avg_4 = strlen($responseJson[0]['4_bedroom_price_avg']);
        
        $response->assertStatus(200);
        $this->assertTrue($median_house_price > 0);
        $this->assertTrue($price_movement_five_years > 0);
        $this->assertTrue($bedroom_price_avg_2 > 0);
        $this->assertTrue($bedroom_price_avg_3 > 0);
        $this->assertTrue($bedroom_price_avg_4 > 0);
    
    }

    public function testHousing3()
    {

        $suburb = "notatown";
        Log::debug("Testing: http://hostname/api/suburb/$suburb");
        $response = $this->json('GET', "/api/suburb/$suburb");

        
        $response->assertStatus(403);

    
    }

    
}

