<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;

class GeneralTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testGeneral1()
    {
        $postcode = "East Albury";
        Log::debug("Testing: http://hostname/api/suburb/$postcode");
        $response = $this->json('GET', "/api/suburb/$postcode");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "postcode" => 2640,
                "state" => "NSW",
            ]
        );
    }

    public function testGeneral2()
    {
        $postcode = 2141;
        Log::debug("Testing: http://hostname/api/suburb/postcode/$postcode");
        $response = $this->json('GET', "/api/suburb/postcode/$postcode");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "state" => "NSW",
                "suburb" => "Rookwood",
            ]
        );
    }

    public function testGeneral3()
    {
        $postcode = 2077;
        Log::debug("Testing: http://hostname/api/suburb/postcode/$postcode");
        $response = $this->json('GET', "/api/suburb/postcode/$postcode");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "median_income" => 38792,
                "suburb" => "Hornsby",
                "lat" => "-33.70",
                "lng" => "151.10",
            ]
        );
    }

    public function testGeneral4()
    {
        $postcode = "Mudgee";
        Log::debug("Testing: http://hostname/api/suburb/$postcode");
        $response = $this->json('GET', "/api/suburb/$postcode");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "postcode" => 2850,
                "population" => 10923,
                "lat" => "-32.61",
                "lng" => "149.57",
            ]
        );
    }

    public function testGeneral5()
    {
        $postcode = "Mudgeeraba";
        Log::debug("Testing: http://hostname/api/suburb/$postcode");
        $response = $this->json('GET', "/api/suburb/$postcode");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "postcode" => 4213,
                "state" => "QLD",
                "population" => 13624,
            ]
        );
    }

    public function testGeneral6()
    {
        $postcode = 3678;
        Log::debug("Testing: http://hostname/api/suburb/postcode/$postcode");
        $response = $this->json('GET', "/api/suburb/postcode/$postcode");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "state" => "VIC",
                "suburb" => "Oxley",
            ]
        );
    }

    public function testGeneral7()
    {
        $postcode = 5260;
        Log::debug("Testing: http://hostname/api/suburb/postcode/$postcode");
        $response = $this->json('GET', "/api/suburb/postcode/$postcode");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "state" => "SA",
                "suburb" => "Tailem Bend",
                "lat" => "-35.28",
                "lng" => "139.49",
            ]
        );
    }

    public function testGeneral8()
    {
        $postcode = "Netherton";
        Log::debug("Testing: http://hostname/api/suburb/$postcode");
        $response = $this->json('GET', "/api/suburb/$postcode");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "postcode" => 5301,
                "state" => "SA",
                "population" => 40,
            ]
        );
    }

    public function testGeneral9()
    {
        $postcode = 6631;
        Log::debug("Testing: http://hostname/api/suburb/postcode/$postcode");
        $response = $this->json('GET', "/api/suburb/postcode/$postcode");

        $response->assertStatus(200)->assertJsonFragment(
            [
                "suburb" => "Pindar",
                "state" => "WA",
                "median_income" => 67600,
                "lat" => "-28.53",
                "lng" => "115.92",
            ]
        );
    }

}
