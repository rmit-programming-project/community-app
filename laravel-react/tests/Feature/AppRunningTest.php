<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;

class AppRunningTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAppRunning()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testApiRunningGeneral()
    {
        $response = $this->json('GET',"/api/suburb/kambah");
        $response->assertStatus(200);
    }

    public function testApiRunningSS()
    {
        $response = $this->json('GET',"/api/schools/suburb/kambah");
        $response->assertStatus(200);
    }

    public function testApiRunningSP()
    {
        $response = $this->json('GET',"/api/schools/postcode/3000");
        $response->assertStatus(200);
    }

    public function testApiRunningSHS()
    {
        $response = $this->json('GET',"/api/schools/postcode/historic/3000");
        $response->assertStatus(200);
    }

    public function testApiRunningSHP()
    {
        $response = $this->json('GET',"/api/schools/suburb/historic/kambah");
        $response->assertStatus(200);
    }

    public function testApiRunningSPS()
    {
        $response = $this->json('GET',"/api/schools/3000/MELBOURNE");
        $response->assertStatus(200);
    }

    public function testApiRunningWMD()
    {
        $response = $this->json('GET',"/api/monthly-data?lat=-35.386&lng=149.057");
        $response->assertStatus(200);
    }
}
