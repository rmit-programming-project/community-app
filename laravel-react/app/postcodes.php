<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postcodes extends Model
{
    protected $guarded = [];
    protected $table = 'postcodes';
    public $timestamps = false; 
}
