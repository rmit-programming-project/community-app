<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schools extends Model
{
    protected $guarded = [];
    protected $table = 'school_locations';
    public $timestamps = false; 

}
