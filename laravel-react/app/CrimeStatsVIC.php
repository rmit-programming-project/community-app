<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrimeStatsVIC extends Model
{
    protected $guarded = [];
    protected $table = 'VIC_CrimeStats';
    public $timestamps = false; 

}
