<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolsDetails extends Model
{
    protected $guarded = [];
    protected $table = 'school_profiles';
    public $timestamps = false; 
}
