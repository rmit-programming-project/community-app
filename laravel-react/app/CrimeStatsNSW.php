<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrimeStatsNSW extends Model
{
    protected $guarded = [];
    protected $table = 'NSW_CrimeStats';
    public $timestamps = false; 

}
