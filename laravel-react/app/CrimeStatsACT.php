<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrimeStatsACT extends Model
{
    protected $guarded = [];
    protected $table = 'ACT_CrimeStats';
    public $timestamps = false; 

}
