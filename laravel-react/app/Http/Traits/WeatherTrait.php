<?php
namespace App\Http\Traits;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

trait WeatherTrait {
    
    /**
     * get monthly average
     * parameters $lat, $lng
     * return json
     */
    public function getMonthlyAvg($lat=null, $lng=null){
        // define api key
        $api_key = "d6db5197fcf24cdda0791054202204";
        
        // monthly average array
        $monthlyAvg = [];

        
        // if not set latitude and longitude
        $q = $lat.','.$lng;

        // make request
        $client = new Client();
        
        // define months array and 
        $months = [
            ['month' => 'March', 'start_date' => '2020-03-01', 'end_date' => '2020-03-31', 'days' => 31],
            ['month' => 'February', 'start_date' => '2020-02-01', 'end_date' => '2020-02-29', 'days' => 29],
            ['month' => 'January', 'start_date' => '2020-01-01', 'end_date' => '2020-01-31', 'days' => 31],
            ['month' => 'December', 'start_date' => '2019-12-01', 'end_date' => '2019-12-31', 'days' => 31],
            ['month' => 'November', 'start_date' => '2019-11-01', 'end_date' => '2019-11-30', 'days' => 30],
            ['month' => 'October', 'start_date' => '2019-10-01', 'end_date' => '2019-10-31', 'days' => 31],
            ['month' => 'September', 'start_date' => '2019-09-01', 'end_date' => '2019-09-30', 'days' => 30],
            ['month' => 'August', 'start_date' => '2019-08-01', 'end_date' => '2019-08-31', 'days' => 31],
            ['month' => 'July', 'start_date' => '2019-07-01', 'end_date' => '2019-07-31', 'days' => 31],
            ['month' => 'June', 'start_date' => '2019-06-01', 'end_date' => '2019-06-30', 'days' => 30],
            ['month' => 'May', 'start_date' => '2019-05-01', 'end_date' => '2019-05-31', 'days' => 31],
            ['month' => 'April', 'start_date' => '2019-04-01', 'end_date' => '2019-04-30', 'days' => 30],
        ];
        
        // loop through months
        foreach ($months as $i => $month){
            $current = [];
            $start_date = $month['start_date'];
            $end_date = $month['end_date'];
            try {
                $res = $client->request('GET', "http://api.worldweatheronline.com/premium/v1/past-weather.ashx?key=d6db5197fcf24cdda0791054202204&q=$q&format=json&date=$start_date&enddate=$end_date&tp=24");
                if ($res->getStatusCode() == 200) {
                    $received_data = $res->getBody();
                    $received_json = json_decode($received_data, true);

                    // check for weather data
                    $month_data = $received_json['data']['weather'];
                    // Log::debug($received_json['data']['weather']);

                    // set variables to set in foreach loop
                    $avg_temp = 0;
                    $avg_sun = 0;
                    $avg_rain = 0;
                    $rain_days = 0;

                    if($month_data){
                        foreach($month_data as $key => $md){
                            $data = $month_data[$key];
                            
                            // return $data['avgtempC'];
                            $tempnumb = $data['avgtempC'];
                            $sunnumb = $data['sunHour'];
                            $avg_temp = (int)$tempnumb + $avg_temp;
                            $avg_sun = (int)$sunnumb + $avg_sun;
                            
                            // get the first item in array for hourly report
                            $hourly = $data['hourly'][0];
                            $precip = $hourly['precipMM'];
                            $avg_rain = (int)$precip + $avg_rain;
                            
                            // get rain value
                            if($hourly['weatherDesc']){
                                $rain_hour = $hourly['weatherDesc'][0];
                                if(strpos($rain_hour['value'], 'rain') !== false){
                                    $rain_days = (int)1+$rain_days;
                                }
                            }
                            
                            // if the key is last
                            // calculate and push array to $monthlyAvg
                            if($key == count($month_data) - 1){
                                $avg_temp = round($avg_temp/count($month_data), 2);
                                $avg_sun = round($avg_sun/count($month_data), 2);
                                $avg_rain = round($avg_rain/count($month_data), 2);
                                
                                $current = [
                                    'name' => $month['month'], 
                                    'avg_temp' => $avg_temp, 
                                    'avg_sun' => $avg_sun, 
                                    'rain_days' => $rain_days,
                                    'avg_rain' => $avg_rain
                                ];
                                
                                array_push($monthlyAvg, $current);
                            }
                        }
                    }
                }

            } catch (Exception $e){
                return ['error' => 'No data found. Please provide valid latitude and longitude.'];
            }

            if($i == 11 && count($monthlyAvg)>0){
                return $monthlyAvg;
            }
        }
        return ['error' => 'No data found. Please provide valid latitude and longitude.'];

    }

    /**
     * check if a number starts with digit
     */
    public function getSuburbFromPostcode($postcode){
        // get first 2 numbers
        if(is_numeric($postcode)){
            
            // use switch case to go through
            switch($postcode){
                case $postcode >= 2000 && $postcode < 2300:
                    return 'sydney';
                break;
                case $postcode >= 2300 && $postcode < 2599:
                    return 'newcastle';
                break;
                case $postcode >= 2600 && $postcode < 2999:
                    return 'canberra';
                break;
                case $postcode >= 3000 && $postcode < 3200:
                    return 'Melbourne';
                break;
                case $postcode >= 3200 && $postcode < 3800:
                    return 'campbellfield';
                break;
                case $postcode >= 3800 && $postcode < 4000:
                    return 'rosedale';
                break;
                case $postcode >= 4000 && $postcode < 4207:
                    return 'brisbane';
                break;
                case $postcode >= 4207 && $postcode < 4300:
                    return 'southport';
                break;
                case $postcode >= 4300 && $postcode < 5000:
                    return 'townsville';
                break;
                case $postcode >= 5000 && $postcode < 6000:
                    return 'adelaide';
                break;
                case $postcode >= 6000 && $postcode < 6500:
                    return 'mandurah';
                break;
                case $postcode >= 6500 && $postcode < 7000:
                    return 'geraldton';
                break;
                case $postcode >= 7000 && $postcode < 7116:
                    return 'hobart';
                break;
                case $postcode >= 7116 && $postcode < 7139:
                    return 'queenstown';
                break;
                case $postcode >= 7469 && $postcode < 8000:
                    return 'queenstown';
                break;
                case $postcode >= 800 && $postcode < 830:
                    return 'darwin';
                break;
                case $postcode >= 830 && $postcode < 999:
                    return 'palmerston';
                break;
                default:
                    return '';
                break;
            }
        }
    }    

}