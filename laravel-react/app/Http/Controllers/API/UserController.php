<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191',
            'password' => 'required|string|min:6',
        ]);

        return User::create([
            'name' => $request['name'],
            'email' =>  $request['email'],
            'password' =>  bcrypt($request->password),
        ]);
    }

    public function logout()
    {
        Auth::logout();
    }

    public function login(Request $request)
    {
        // validate email and password
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);


        $user = User::where('email', $request->email)->first();
        
        // if user 
        if(!$user) {
            return response(['status' =>'error', 'message' => 'User not Found']); 
        }

        if (Auth::attempt(array('email' => $request->email, 'password' => $request->password))){
                return  $user;
        }
        else {        
            return response(['status' =>'error', 'message' => 'Wrong Credentials']); ;
        }

        

    }
}
