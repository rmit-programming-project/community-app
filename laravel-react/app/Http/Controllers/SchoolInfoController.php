<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schools;
use App\SchoolsDetails;
use Illuminate\Support\Facades\Log;
use Exception;

class SchoolInfoController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

   
    public function index($postcode)
    {
        $pairedSchools = [];
        try {
            $schools = Schools::where('postcode', 'like', $postcode . '%')->get()->toArray();
        } catch (Exception $e) {
            return response($e->getMessage(), 400);
        }
        foreach ($schools as $school) {
            $details = SchoolsDetails::where('acara-sml-id', 'like', $school['acara_id'])
                ->where('calendar-year', 'like', 2019)->get()->toArray();
            $pair = [$school, $details];

            // Log::debug($pair);
            array_push($pairedSchools, $pair);
        }
        if (count($pairedSchools) <= 0){
            return response("Not Found",400)->header('Content-Type', 'application/json');
        }
        // $details = SchoolsDetails::where('acara-sml-id', 'like', $school['acara_id'])->get()->toArray();

        return response($pairedSchools, 200)->header('Content-Type', 'application/json');
    }

    public function Suburb($suburb)
    {
        $pairedSchools = [];
        try {
            $schools = Schools::where('suburb', 'like', $suburb . '%')->get()->toArray();
        } catch (Exception $e) {
            return response($e->getMessage(), 400);
        }
        foreach ($schools as $school) {
            $details = SchoolsDetails::where('acara-sml-id', 'like', $school['acara_id'])
                ->where('calendar-year', 'like', 2019)->get()->toArray();
            $pair = [$school, $details];

            // Log::debug($pair);
            array_push($pairedSchools, $pair);
        }
        if (count($pairedSchools) <= 0){
            return response("Not Found",400)->header('Content-Type', 'application/json');
        }
        // $details = SchoolsDetails::where('acara-sml-id', 'like', $school['acara_id'])->get()->toArray();

        return response($pairedSchools, 200)->header('Content-Type', 'application/json');
    }

    public function PostcodeSuburb($postcode,$suburb)
    {
        $pairedSchools = [];
        try {
            $schools = Schools::where('postcode', 'like', $postcode . '%')
            ->where('suburb', 'like', $suburb . '%')->get()->toArray();
        } catch (Exception $e) {
            return response($e->getMessage(), 400);
        }
        foreach ($schools as $school) {
            $details = SchoolsDetails::where('acara-sml-id', 'like', $school['acara_id'])
                ->where('calendar-year', 'like', 2019)->get()->toArray();
            $pair = [$school, $details];

            // Log::debug($pair);
            array_push($pairedSchools, $pair);
        }
        if (count($pairedSchools) <= 0){
            return response("Not Found",400)->header('Content-Type', 'application/json');
        }
        // $details = SchoolsDetails::where('acara-sml-id', 'like', $school['acara_id'])->get()->toArray();

        return response($pairedSchools, 200)->header('Content-Type', 'application/json');
    }

    public function HistoricPostcode($postcode)
    {

        $pairedSchools = [];
        try {
            $schools = Schools::where('postcode', 'like', $postcode . '%')->get()->toArray();
        } catch (Exception $e) {
            return response($e->getMessage(), 400);
        }
        foreach ($schools as $school) {


            $details = SchoolsDetails::where('acara-sml-id', 'like', $school['acara_id'])->get()->toArray();
            $pair = [$school, $details];

            // Log::debug($pair);
            array_push($pairedSchools, $pair);
        }
        if (count($pairedSchools) <= 0){
            return response("Not Found",400)->header('Content-Type', 'application/json');
        }
        // $details = SchoolsDetails::where('acara-sml-id', 'like', $school['acara_id'])->get()->toArray();

        return response($pairedSchools, 200)->header('Content-Type', 'application/json');
    }


    public function HistoricSuburb($suburb)
    {
        
        $pairedSchools = [];
        try {
            $schools = Schools::where('suburb', 'like', $suburb . '%')->get()->toArray();
        } catch (Exception $e) {
            return response($e->getMessage(), 400);
        }
        foreach ($schools as $school) {


            $details = SchoolsDetails::where('acara-sml-id', 'like', $school['acara_id'])->get()->toArray();
            $pair = [$school, $details];

            // Log::debug($pair);
            array_push($pairedSchools, $pair);
        }
        if (count($pairedSchools) <= 0){
            return response("Not Found",400)->header('Content-Type', 'application/json');
        }
        // $details = SchoolsDetails::where('acara-sml-id', 'like', $school['acara_id'])->get()->toArray();

        return response($pairedSchools, 200)->header('Content-Type', 'application/json');
    }


}
