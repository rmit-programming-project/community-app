<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Traits\WeatherTrait;
use Illuminate\Support\Facades\Log;

class WeatherApiController extends Controller
{
    use WeatherTrait;

    public function getSuburbData(Request $request){
        /**
         * This below is a demo URL of a GET request to get weather data for a suburb
         */
        // http://localhost:8000/api/suburb-data?lat=37&lng=144
        
        $lat = $request->lat;
        $lng = $request->lng;
        $weatherAPIKey = "20f0e26cc612b06b4728a4541d178edb";
        $client = new Client();
        if(isset($lat) && isset($lng)){
            try {
                // get using zip code
                // https://api.openweathermap.org/data/2.5/weather?zip=$postcode,61&appid=$weatherAPIKey
                // get using lat, lng
                // https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$lng&appid=$weatherAPIKey
                $res = $client->request('GET', "https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$lng&appid=$weatherAPIKey&units=metric", [
                    'headers' => [
                        'auth-key' =>  $weatherAPIKey
                    ]
                ]);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $message = "Please include latitude and longitude in request. \r\nRetuned: $error";
                return json_encode(['error' => $message]);
            }

            if ($res->getStatusCode() == 200) {
                $received_data = $res->getBody();
                $received_json = json_decode($received_data, true);
                return $received_json;
            }

        }else {
            return ['error' => 'Please provide latitude and longitude.'];   
        }        

    }

    /**
     * Get monthly average
     */
    public function getMonthlyData(Request $request){
       
        if($request->lat && $request->lng){
            return $this->getMonthlyAvg($request->lat, $request->lng);
        } else {
            return ['error' => 'No data found. Please provide valid latitude and longitude.'];
        }
    }  
    
     /**
     * Air quality
     */
    public function getAirQuality(Request $request){
        // key
        $key = '166e758659d31ccd6f500cbd736f92ee1e6a736d';

        // make request
        $client = new Client();

        // suburb variable
        $suburb = '';

        if($request->postcode){
            $suburb = $this->getSuburbFromPostcode($request->postcode);
        }

        // set empty array
        $air_quality = [];
        
        try {
            // url = https://api.waqi.info/feed/bundoora/?token=166e758659d31ccd6f500cbd736f92ee1e6a736d
            $res = $client->request('GET', "https://api.waqi.info/feed/$suburb/?token=$key");
            
            if ($res->getStatusCode() == 200) {
                $responses = json_decode($res->getBody(), true);
                

                if($responses && isset($responses['data'])){
                    $r = $responses['data'];
                    $air = '';
                    if($r['aqi'] < 50){
                        $air = 'Good';
                    } elseif($r['aqi'] < 100 && $r['aqi'] > 50){
                        $air = 'moderate';
                    } elseif($r['aqi'] < 150 && $r['aqi'] > 100){
                        $air = 'Unhealthy for sensitive groups';
                    } elseif($r['aqi'] < 200 && $r['aqi'] > 150){
                        $air = 'unhealthy';
                    } elseif($r['aqi'] < 300 && $r['aqi'] > 200){
                        $air = 'very unhealthy';
                    } elseif($r['aqi'] < 300){
                        $air = 'hazardous';
                    } 
                    $air_quality['aqi'] = $air;
                } else {
                    $air_quality['error'] = 'No data found. Please provide a valid postcode.';
                }
            }
        } catch(Exception $e){
            $air_quality['error'] = 'No data found. Please provide a valid postcode.';
        }

        return $air_quality;

    }   
}
