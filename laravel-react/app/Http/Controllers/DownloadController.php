<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function index(Request $request)
    {

        Log::debug('message');
        if($request->isMethod('post')) {
            Log::debug('message');
            $data = json_decode($request['data'],true);
            
            
        }
        $returnedData = array(
            "general-data" => $data['general'],
            "school-data" => $data['school'],
            "weather-data" => $data['weather'],
            "crime-data" => $data['crime'],
        );

        Log::debug($returnedData);

        return response($returnedData,200)->header('Content-Type', 'text/plain');
    }
}
