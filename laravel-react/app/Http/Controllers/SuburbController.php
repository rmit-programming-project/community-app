<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class SuburbController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function compare($postcode1, $postcode2)
    {
        
        return compact('suburb1', 'suburb2');
        
    }

    public function addSuburbToFavourites($suburb)
    {
       return auth()->user()->suburbs()->toggle($suburb);
    }

    public function getFavouriteSuburbs($userId)
    {
        $user = User::find($userId);

        if($user) {
        return $suburbs = $user->suburbs;
        } else
        {return ['message' => 'You haven`t got a suburb in favourites.'];}
    }   

}
