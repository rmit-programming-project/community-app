<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suburb;
use Illuminate\Support\Facades\Log;


class ApidataController extends Controller
{


    public function suburbInfo($suburb)
    {

        $suburbs = Suburb::where('suburb', 'like', $suburb . '%')->get()->toArray();
        if(count($suburbs)==0) { return response("not found",403)->header('Content-Type', 'text/plain'); }
        $suburbsReturn = [];
        foreach ($suburbs as $suburb) {
            // Log::debug($suburb);
            $suburb['liviblity_score'] = $this->livabilityScore($suburb['population'], $suburb['sqkm'], $suburb['median_income'], $suburb['median_house_price']);
            array_push($suburbsReturn, $suburb);
        }
        return $suburbsReturn;
    }

    public function suburbInfoPostcode($postcode)
    {

        $suburbs = Suburb::where('postcode', 'like', $postcode . '%')->get()->toArray();
        if(count($suburbs)==0) { return response("not found",403)->header('Content-Type', 'text/plain'); }
        $suburbsReturn = [];
        foreach ($suburbs as $suburb) {
            // Log::debug($suburb);
            $suburb['liviblity_score'] = $this->livabilityScore($suburb['population'], $suburb['sqkm'], $suburb['median_income'], $suburb['median_house_price']);
            array_push($suburbsReturn, $suburb);
        }
        return $suburbsReturn;
    }

    private function livabilityScore($population, $area, $medianIncome, $medianHousePrice)
    {

        // Density factor
        $density = $population / $area;

        switch ($density) {
            case ($density < 2000):
                $densityFactor = 100;
                break;
            case ($density < 3500):
                $densityFactor = 95;
                break;
            case ($density < 5000):
                $densityFactor = 90;
                break;
            case ($density < 6000):
                $densityFactor = 85;
                break;
            case ($density < 7000):
                $densityFactor = 80;
                break;
            case ($density <= 7000):
                $densityFactor = 75;
                break;
            default:
                $densityFactor = 70;
        }

        // Affordability factor

        // 2,000,000 - 200,000 = 1,800,000 / 100 = 18,000 = 1%
        // $medianPrice - 650,000 = $newValue
        $medianPriceCalculation = $medianHousePrice - 200000;
        // $newValue / 18,000 = 1-100% livibility factor by affordability 
        $affordibilityFactor = $medianPriceCalculation / 18000;


        // Income Factor 
        // highest 90948 / 100 - 909.48
        // lowest 0
        $incomeFactor = $medianIncome / 910;

        // TOTAL
        $factorTotal = ($affordibilityFactor + $densityFactor + $incomeFactor) / 3;

        return $factorTotal;
    }

    // public function getRealesateInfo($suburb)
    // {
    //     $curl = curl_init();
    //     curl_setopt_array($curl, array(
    //     CURLOPT_URL => "https://data.sa.gov.au/data/api/3/action/datastore_search?resource_id=d9077f5d-e005-4f93-8389-6f2759cdf7fa&limit=5",
    //     CURLOPT_RETURNTRANSFER => true,
    //     CURLOPT_ENCODING => "",
    //     CURLOPT_MAXREDIRS => 10,
    //     CURLOPT_TIMEOUT => 30,
    //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //     CURLOPT_CUSTOMREQUEST => "GET",
    //     CURLOPT_POSTFIELDS => "",
    //     CURLOPT_HTTPHEADER => array(
    //         "Content-Type: application/json",
    //         "cache-control: no-cache"
    //     ),
    //     ));

    //     $response = curl_exec($curl);
    //     $err = curl_error($curl);

    //     $data = json_decode($response, true);

    //     // do something with the data
    //     unset($data["help"]);
    //     unset($data["success"]);
    //     unset($data["result"]['resource_id']);
    //     unset($data["result"]["_links"]);
    //     unset($data["result"]["limit"]);
    //     unset($data["result"]["total"]);


    //     return $data;
    //     }

}
