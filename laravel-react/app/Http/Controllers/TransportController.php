<?php

namespace App\Http\Controllers;
// use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;

use App\TransportStops;


use Exception;

use GuzzleHttp\Client;


use App\Suburb;
use App\postcodes;



class TransportController extends Controller
{
    public function index($postcode){


        

        #Init array to collect all location details for return. 
        $collection = array();

        #Get entered postcode from API
        // $postcode = 2902;

    
        $totalcount = 0;

        // $client = new Client();
   
        $request = Request::create("/api/details?postcode=$postcode", 'GET');
        $data = app()->handle($request);
        
        $received_json = $data;

        $instance = json_decode(Route::dispatch($request)->getContent(),true);

        $locationsDetails = Suburb::where('postcode', 'like', $postcode.'%')
                            ->select('postcode','suburb','sqkm')->get()->toArray();



        foreach($instance as $data){
            Log::debug($data);
            foreach($locationsDetails as $location){
                if(strtolower($data['location']) === strtolower($location['suburb'])){
                    Log::debug($data);
                    Log::debug($location);
                    $thislocations = array(
                        "postcode" => $data['postcode'],
                        "suburb" => $data['location'],
                        "lat" => $data['latitude'],
                        "lng" => $data['longitude'],
                        "sqkm" => $location['sqkm'],
                    );
                    array_push($collection,$thislocations);
                }

                // $theResponse = $received_json;
            }
            // Log::debug($collection);

        }
        $edgeCollection = array();
        foreach($collection as $suburb){
            $x = $this->getEdges($suburb['lat'],$suburb['lng'],$suburb['sqkm']);
            array_push($edgeCollection,array(
                'suburb' => $suburb['suburb'],
                'origLat' => $suburb['lat'],
                'origLng' => $suburb['lng'],
                'edges'=>$x
            ));
        }
        $allstops = array();
        foreach($edgeCollection as $suburb){
            Log::debug($suburb['edges']['westLng']);
             $stops = TransportStops::wherebetween('stop_lon', [$suburb['edges']['westLng'],$suburb['edges']['eastLng']])->
                                      wherebetween('stop_lat', [$suburb['edges']['southLat'],$suburb['edges']['northLat']])->                      
                                    // where('stop_lon', '<', $suburb['edges']['eastLng'])->
                                    // where('stop_lat', '>', $suburb['edges']['southLat'])->
                                    // where('stop_lat', '<', $suburb['edges']['northLat'])->

                                    get()->toArray();
             
             $totalcount += count($stops);
             array_push($allstops,$stops);
             
        }
        Log::debug($allstops);
        // $sqkm = postcodes::where('postcode', 'like', $postcode.'%')->get()->toArray();

        $theResponse = array(
            "postcode" => $postcode,
            "totalCount" => $totalcount,
            "bounds" => $edgeCollection,
            "stops" => $allstops,

        );

       
        return $theResponse;
        }

        

    

    private function getEdges($lat,$lng,$size){
        $distance = ($size*1000)/4;
        $negDistance = -$distance;
        $northLat = $this->newLat($lat,$distance);
        $southLat = $this->newLat($lat,$negDistance);
        $eastLng = $this->newLng($lng,$lat,$distance);
        $westLng = $this->newLng($lng,$lat,$negDistance);
        $package = array(
            'westLng' => $westLng,
            'eastLng' => $eastLng,
            'northLat' => $northLat,
            'southLat' => $southLat
        );
        return $package;

    }


    private function newLat($lat,$meters){
        //radius of the earth in kilometer
        $earth = 6378.137;
        $pi = pi();
        $m = (1 / ((2 * $pi / 360) * $earth)) / 1000;  //1 meter in degree
    
        $new_latitude = $lat + ($meters * $m);
        return $new_latitude;
    }

    private function newLng($lng,$lat,$meters){
        //radius of the earth in kilometer
        $earth = 6378.137;
        $pi = pi();
        $m = (1 / ((2 * $pi / 360) * $earth)) / 1000;  //1 meter in degree
        $new_longitude = $lng + ($meters * $m) / cos($lat * ($pi / 180));
        return $new_longitude;

    }
}
