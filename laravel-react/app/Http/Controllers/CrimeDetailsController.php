<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use Psr\Http\Message\ResponseInterface;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Http\Client\Exception;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\BadResponseException;

use App\CrimeStatsACT;
use App\CrimeStatsNSW;
use App\CrimeStatsVIC;
use App\Suburb;
use App\postcodes;


class CrimeDetailsController extends Controller
{
    public function index($postcode){   
        $suburb = [];
        try {
            //Search postcodes table for entries which match the inputted post code
            //add results to array
            $suburbs = postcodes::where('postcode', 'like', $postcode.'%')->get()->toArray();
            // Log::debug($suburbs);
        } 
        catch(exception $e){
            return response($e->getMessage(), 400);
        }
// foreach ($suburbs as $thisSuburb){
//       //capture all suburb elements into an array
//     array_push($suburb, $thisSuburb['suburb']);
// }
// Log::debug($suburb);
        

        //Return Error is postcode doesnt exist
        if(count($suburbs)==0) return response("postcode doesnt exist", 404);
    
        //Check the STATE and pass the array to correct function. 
        if ($suburbs[0]['state_code'] == 'ACT'){
            $suburb = $this->ActCrime($suburbs);
        }
        elseif ($suburbs[0]['state_code'] == 'NSW'){
            $suburb = $this->NSWCrime($postcode);
        }
        elseif ($suburbs[0]['state_code'] == 'VIC'){
            $suburb = $this->VICCrime($postcode);
        }
        elseif ($suburbs[0]['state_code'] == 'NT'){
            $suburb = $this->NTCrime($suburbs);
        }
        elseif ($suburbs[0]['state_code'] == 'SA'){
            $suburb = $this->SACrime($postcode);
        }
        elseif ($suburbs[0]['state_code'] == 'WA'){
            $suburb = $this->WACrime($suburbs);
        }
        elseif ($suburbs[0]['state_code'] == 'TAS'){
            $suburb = $this->TASCrime($suburbs);
        }
        elseif ($suburbs[0]['state_code'] == 'QLD'){
            $suburb = $this->QLDCrime($suburbs);
        }
        else {return response('invalid code', 400);}

        //Return result. 
        return response($suburb, 200)->header('Content-Type', 'application/json');
    }
    private function ActCrime($suburbs){
        Log::debug('Called');
        //The year we want to search for. Can change to parameter
        $year = 2019;
        
        //track total number for tracked crimes in postcode.
        $totalCrimes = 0;
        $totalMurders = 0;
        $totalAssault = 0;
        $totalBurglary = 0;
        $totalTheft = 0;
        
        //search through the array of suburbs which match the postcode
        foreach($suburbs as $suburb){

            //Collect all crimes for the current suburb
            $crimes = CrimeStatsACT::where('suburb','like',$suburb['suburb'])
                    ->where('calendar-year','like',$year)
                    ->get()->toArray();
            //Collect all Murders for the current suburb
            $murders = CrimeStatsACT::where('suburb','like',$suburb['suburb'])
                        ->where('crime-type','like','Homicide'.'%')
                        ->where('calendar-year','like',$year)
                        ->get()->toArray();

            //Collect all Assaults for the current suburb
            $assaults = CrimeStatsACT::where('suburb','like',$suburb['suburb'].'%')
                        ->where('crime-type','like','Assault'.'%')
                        ->where('calendar-year','like',$year)
                        ->get()->toArray();

            //Collect all burglarys for the current suburb
            $burglarys = CrimeStatsACT::where('suburb','like',$suburb['suburb'].'%')
                        ->where('crime-type','like','Burglary'.'%')
                        ->where('calendar-year','like',$year)
                        ->get()->toArray();
            
            //Collect all Theifts for the current suburb
            $thefts = CrimeStatsACT::where('suburb','like',$suburb['suburb'].'%')
                        ->where('crime-type','like','Robbery'.'%')
                        ->where('calendar-year','like',$year)
                        ->get()->toArray();

            
            //count all the totals for the tracked categories and 
            //add them to the grandtotal for the postcode
            foreach($crimes as $crime){
                $totalCrimes +=  $crime['amount_crimes'];
             }

            foreach($murders as $murder){
                $totalMurders +=  $murder['amount_crimes'];
             }
             foreach($assaults as $assault){
                 $totalAssault +=  $assault['amount_crimes'];
              }
              foreach($burglarys as $burglary){
                 $totalBurglary +=  $burglary['amount_crimes'];
              }
              foreach($thefts as $theft){
                 $totalTheft +=  $assault['amount_crimes'];
              }
            
            //may need to compare totalcrimes with population for better reading

       }//foreach suburbs
        // Log::debug($totalCrimes);
        // Log::debug($totalMurders);
        // Log::debug($totalAssault);
        // Log::debug($totalBurglary);
        // Log::debug($totalTheif);

        //build array for return with totals for all crimes. 
        $result = array(
            'totalCrimes' => $totalCrimes,
            'totalMurders' => $totalMurders,
            'totalAssault' => $totalAssault,
            'totalBurgalary' => $totalBurglary,
            'totalTheft' => $totalTheft,   
        );
        // Log::debug($result);
        return $result;

    } //ACT CRIMES

        
    private function NSWCrime($postcode){
        //The year we want to search for. Can change to parameter
        $year = 2019;
        
        //track total number for tracked crimes in postcode.
        $totalCrimes = 0;
        $totalMurders = 0;
        $totalAssault = 0;
        $totalBurglary = 0;
        $totalTheft = 0;
        
        //search through the array of suburbs which match the postcode
        

            //Collect all crimes for the current suburb
            $crimes = CrimeStatsNSW::where('postcode','like',$postcode)
                    ->where('calendar-year','like',$year)
                    ->get()->toArray();
            //Collect all Murders for the current suburb
            $murders = CrimeStatsNSW::where('postcode','like',$postcode)
                        ->where('crime-type','like','Homicide'.'%')
                        ->where('calendar-year','like',$year)
                        ->get()->toArray();

            //Collect all Assaults for the current suburb
            $assaults = CrimeStatsNSW::where('postcode','like',$postcode.'%')
                        ->where('crime-type','like','Assault'.'%')
                        ->where('calendar-year','like',$year)
                        ->get()->toArray();

            //Collect all burglarys for the current suburb
            $burglarys = CrimeStatsNSW::where('postcode','like',$postcode.'%')
                        ->where('crime-type','like','Robbery'.'%')
                        ->where('calendar-year','like',$year)
                        ->get()->toArray();
            
            //Collect all Theifts for the current suburb
            $thefts = CrimeStatsNSW::where('postcode','like',$postcode.'%')
                        ->where('crime-type','like','Theft'.'%')
                        ->where('calendar-year','like',$year)
                        ->get()->toArray();

            
            //count all the totals for the tracked categories and 
            //add them to the grandtotal for the postcode
            foreach($crimes as $crime){
                $totalCrimes +=  $crime['amount_crimes'];
             }

            foreach($murders as $murder){
                $totalMurders +=  $murder['amount_crimes'];
             }
             foreach($assaults as $assault){
                 $totalAssault +=  $assault['amount_crimes'];
              }
              foreach($burglarys as $burglary){
                 $totalBurglary +=  $burglary['amount_crimes'];
              }
              foreach($thefts as $theft){
                 $totalTheft +=  $assault['amount_crimes'];
              }
            
            //may need to compare totalcrimes with population for better reading

       //foreach suburbs
        // Log::debug($totalCrimes);
        // Log::debug($totalMurders);
        // Log::debug($totalAssault);
        // Log::debug($totalBurglary);
        // Log::debug($totalTheif);

        //build array for return with totals for all crimes. 
        $result = array(
            'totalCrimes' => $totalCrimes,
            'totalMurders' => $totalMurders,
            'totalAssault' => $totalAssault,
            'totalBurgalary' => $totalBurglary,
            'totalTheft' => $totalTheft,   
        );
        // Log::debug($result);
        return $result;
    }

    private function VICCrime($postcode){
        //The year we want to search for. Can change to parameter
        $year = 2019;
        
        //track total number for tracked crimes in postcode.
        $totalCrimes = 0;
        $totalMurders = 0;
        $totalAssault = 0;
        $totalBurglary = 0;
        $totalTheft = 0;
        
        //search through the array of suburbs which match the postcode

            //Collect all crimes for the current suburb
            $crimes = CrimeStatsVIC::where('postcode','like',$postcode)
                    ->where('year','like',$year)
                    ->get()->toArray();
            //Collect all Murders for the current suburb
            $murders = CrimeStatsVIC::where('postcode','like',$postcode)
                        ->where('crime-type','like','Homicide'.'%')
                        ->where('year','like',$year)
                        ->get()->toArray();

            //Collect all Assaults for the current suburb
            $assaults = CrimeStatsVIC::where('postcode','like',$postcode.'%')
                        ->where('crime-type','like','A20Assaultandrelatedoffences'.'%')
                        ->where('year','like',$year)
                        ->get()->toArray();

            //Collect all burglarys for the current suburb
            $burglarys = CrimeStatsVIC::where('postcode','like',$postcode.'%')
                        ->where('crime-type','like','B30Burglary/Breakandenter'.'%')
                        ->where('year','like',$year)
                        ->get()->toArray();
            
            //Collect all Theifts for the current suburb
            $thefts = CrimeStatsVIC::where('postcode','like',$postcode.'%')
                        ->where('crime-type','like','B40Theft'.'%')
                        ->where('year','like',$year)
                        ->get()->toArray();

            
            //count all the totals for the tracked categories and 
            //add them to the grandtotal for the postcode
            foreach($crimes as $crime){
                $totalCrimes +=  $crime['amount_crimes'];
             }

            foreach($murders as $murder){
                $totalMurders +=  $murder['amount_crimes'];
             }
             foreach($assaults as $assault){
                 $totalAssault +=  $assault['amount_crimes'];
              }
              foreach($burglarys as $burglary){
                 $totalBurglary +=  $burglary['amount_crimes'];
              }
              foreach($thefts as $theft){
                 $totalTheft +=  $assault['amount_crimes'];
              }
            
            //may need to compare totalcrimes with population for better reading
            //build array for return with totals for all crimes. 
        $result = array(
            'totalCrimes' => $totalCrimes,
            'totalMurders' => 'Data Not Available',
            'totalAssault' => $totalAssault,
            'totalBurgalary' => $totalBurglary,
            'totalTheft' => $totalTheft,   
        );
        // Log::debug($result);
        return $result;
    }
    private function NTCrime($suburb){

        $result = array(
            'totalCrimes' => "Not Found",
            'totalMurders' => "Not Found",
            'totalAssault' => "Not Found",
            'totalBurgalary' => "Not Found",
            'totalTheft' => "Not Found",   
        );
        return $result;

    }
    private function SACrime($postcode){
        //https://data.sa.gov.au/data/api/3/action/datastore_search?resource_id=590083cd-be2f-4a6c-871e-0ec4c717717b&q=5016
        //https://data.sa.gov.au/data/api/3/action/datastore_search?resource_id=590083cd-be2f-4a6c-871e-0ec4c717717b&q={POSTCODE}
        $client = new Client();
        
        //The year to search for. 
        $year = '2019';

        //tracked crimes total. 
        $totalCrimes = 0;
        $totalMurders = 0;
        $totalAssault = 0;
        $totalBurglary = 0;
        $totalTheft = 0;

        // Log::debug("IN SA CRIME");

        //collection of valid crime data. 
        $allresposes = [];

        //Make a request to the SA API for the input postcode.
       
            try{
                $res = $client->get("https://data.sa.gov.au/data/api/3/action/datastore_search?resource_id=590083cd-be2f-4a6c-871e-0ec4c717717b&q={$postcode}",['http_errors' => false]);
                Log::debug($res->getStatusCode());

                if ($res->getStatusCode() == 200) {

                    #transform received data to JSON. 
                    $received_data = $res->getBody();
                    $received_json = json_decode($received_data, true);
                    
                    //add valid result to collection of crime data. 
                    array_push($allresposes,$received_json);
                }
            }catch (GuzzleHttp\Exception\ServerException  $e) {
                // $response = $e->getResponse();
                // $responseBodyAsString = $response->getBody()->getContents();
            }catch(Execption $e){
            }

        //For every suburb in collection
        foreach($allresposes as $suburb){
            
            //For every offence in suburb.
            foreach($suburb['result']['records'] as $offence){
                // Log::debug($offence);

                // if the offence is this year gather stats
                if(strpos($offence['Reported Date'],$year)!== FALSE){

                    //look for burgalries for this suburb and add to the total for postcode. 
                    if(strpos($offence['Offence Level 2 Description'],"SERIOUS CRIMINAL TRESPASS") !== FALSE){
                        $totalBurglary += $offence['Offence count']; 
                    }
                    //look for assults for this suburb and add to the total for postcode. 
                    elseif(strpos($offence['Offence Level 2 Description'],"ACTS INTENDED TO CAUSE INJURY") !== FALSE)
                    {
                        $totalAssault += $offence['Offence count']; 
                    }
                    //look for theifs for this suburb and add to the total for postcode. 
                    elseif(strpos($offence['Offence Level 2 Description'],"THEFT AND RELATED OFFENCES") !== FALSE)
                    {
                        $totalTheft += $offence['Offence count']; 
                    }
                    //look for murders for this suburb and add to the total for postcode. 
                    elseif(strpos($offence['Offence Level 2 Description'],"Homicide") !== FALSE)
                    {
                        $totalMurders += $offence['Offence count']; 
                    }

                    //Add TotalAnnual for each crime to grand total for postcode. 
                    $totalCrimes += $offence['Offence count'];
                }
            }
            
            
        }

        //make array for with total stats to return
        $result = array(
            'totalCrimes' => $totalCrimes,
            'totalMurders' => $totalMurders,
            'totalAssault' => $totalAssault,
            'totalBurgalary' => $totalBurglary,
            'totalTheft' => $totalTheft,   
        );

        // Log::debug($totalCrimes);
        // Log::debug($totalMurders);
        // Log::debug($totalAssault);
        // Log::debug($totalTheif);
        // Log::debug($totalBurglary);
        return $result;

    }
    private function WACrime($suburbs){
        
        $client = new Client();
        
        //The year to search for. 
        $year = '2018-19';

        //tracked crimes total. 
        $totalCrimes = 0;
        $totalMurders = 0;
        $totalAssault = 0;
        $totalBurglary = 0;
        $totalTheft = 0;

        //collection of valid crime data. 
        $allresposes = [];

        //Make a request to the WA API for all suburbs which match the input postcode.
        foreach($suburbs as $suburb){
            try{
                $res = $client->get("https://www.police.wa.gov.au/apiws/CrimeStatsApi/GetLocalityCrimeStats/{$suburb['suburb']}",['http_errors' => false]);
                // Log::debug($res->getStatusCode());

                if ($res->getStatusCode() == 200) {

                    #transform received data to JSON. 
                    $received_data = $res->getBody();
                    $received_json = json_decode($received_data, true);
                    
                    //add valid result to collection of crime data. 
                    array_push($allresposes,$received_json);
                }
            }catch (GuzzleHttp\Exception\ServerException  $e) {
                continue;
                // $response = $e->getResponse();
                // $responseBodyAsString = $response->getBody()->getContents();
            }catch(Execption $e){
                continue;
            }
        }
        //For every suburb in collection
        foreach($allresposes as $suburb){
            //For every offence in suburb.
            foreach($suburb as $offence){

                //if the offence is this year gather stats
                if($offence['FinancialYear']==$year){

                    //look for burgalries for this suburb and add to the total for postcode. 
                    if(strpos($offence['Offence'],"Burglary") !== FALSE){
                        $totalBurglary += $offence['TotalAnnual']; 
                    }
                    //look for assults for this suburb and add to the total for postcode. 
                    elseif(strpos($offence['Offence'],"Assault") !== FALSE)
                    {
                        $totalAssault += $offence['TotalAnnual']; 
                    }
                    //look for theifs for this suburb and add to the total for postcode. 
                    elseif(strpos($offence['Offence'],"Stealing") !== FALSE)
                    {
                        $totalTheft += $offence['TotalAnnual']; 
                    }
                    //look for murders for this suburb and add to the total for postcode. 
                    elseif(strpos($offence['Offence'],"Homicide") !== FALSE)
                    {
                        $totalMurders += $offence['TotalAnnual']; 
                    }

                    //Add TotalAnnual for each crime to grand total for postcode. 
                    $totalCrimes += $offence['TotalAnnual'];
                }
            }
            
            
        }

        //make array for with total stats to return
        $result = array(
            'totalCrimes' => $totalCrimes,
            'totalMurders' => $totalMurders,
            'totalAssault' => $totalAssault,
            'totalBurgalary' => $totalBurglary,
            'totalTheft' => $totalTheft,   
        );

        // Log::debug($totalCrimes);
        // Log::debug($totalMurders);
        // Log::debug($totalAssault);
        // Log::debug($totalTheif);
        // Log::debug($totalBurglary);
        return $result;

    }
    private function TASCrime($suburb){
        $result = array(
            'totalCrimes' => "Not Found",
            'totalMurders' => "Not Found",
            'totalAssault' => "Not Found",
            'totalBurgalary' => "Not Found",
            'totalTheft' => "Not Found",   
        );
        return $result;

    }
    private function QLDCrime($suburbs){
        //https://a5c7zwf7e5.execute-api.ap-southeast-2.amazonaws.com/dev/offences?startDate=01-01-2019&endDate=12-01-2019&format=JSON&locationType=SUBURB&locationName=Pinkenba
        //https://a5c7zwf7e5.execute-api.ap-southeast-2.amazonaws.com/dev/offences?startDate=01-01-2019&endDate=12-01-2019&format=JSON&locationType=SUBURB&locationName={SUBURBNAME}
        $client = new Client();
        
        //The year to search for. 
        $year = '2019';

        //tracked crimes total. 
        $totalCrimes = 0;
        $totalMurders = 0;
        $totalAssault = 0;
        $totalBurglary = 0;
        $totalTheft = 0;

        //collection of valid crime data. 
        $allresposes = [];
        

        //Make a request to the QLD API for all suburbs which match the input suburb.
        foreach($suburbs as $suburb){
            try{
                $res = $client->get("https://a5c7zwf7e5.execute-api.ap-southeast-2.amazonaws.com/dev/offences?startDate=01-01-2019&endDate=12-01-2019&format=JSON&locationType=SUBURB&locationName={$suburb['suburb']}",['http_errors' => false]);
                // Log::debug($res->getStatusCode());

                if ($res->getStatusCode() == 200) {

                    #transform received data to JSON. 
                    $received_data = $res->getBody();
                    $received_json = json_decode($received_data, true);
                    
                    //add valid result to collection of crime data. 
                    array_push($allresposes,$received_json);
                }
            }catch (GuzzleHttp\Exception\ServerException  $e) {
                continue;
                // $response = $e->getResponse();
                // $responseBodyAsString = $response->getBody()->getContents();
            }catch(Execption $e){
                continue;
            }
        }
        
        //For every suburb in collection
        foreach($allresposes as $suburb){
            
            // Log::debug($allresposes);
            // Log::debug(count($allresposes));
            
            //For every offence in suburb.
            foreach($suburb as $offence){
                
                
                //if the offence is this year gather stats
                if(strpos($offence['Date'],$year)!== FALSE){
                    // Log::debug($offence['Date']);
                    // Log::debug($offence);
                    //look for burgalries for this suburb and add to the total for postcode. 
                    if(strpos($offence['Type'],"Other Theft (excl. Unlawful Entry)") !== FALSE)
                    {
                        // Log::debug($offence);
                        // Log::debug($totalTheft);
                        $totalTheft++;                        
                    }
                    //look for assults for this suburb and add to the total for postcode. 
                    elseif(strpos($offence['Type'],"Assault") !== FALSE)
                    {
                        $totalAssault++; 
                    }
                    //look for theifs for this suburb and add to the total for postcode. 
                    elseif(strpos($offence['Type'],"Unlawful Entry") !== FALSE)
                    {
                        
                        $totalBurglary++;  
                        
                    }
                    //look for murders for this suburb and add to the total for postcode. 
                    elseif(strpos($offence['Type'],"Homicide") !== FALSE)
                    {
                        $totalMurders++; 
                    }
                    

                    //Add TotalAnnual for each crime to grand total for postcode. 
                    $totalCrimes = $totalBurglary;
                    $totalCrimes += $totalAssault;
                    $totalCrimes += $totalTheft;
                    $totalCrimes += $totalMurders;
                }
            }
            
            
        }

        //make array for with total stats to return
        $result = array(
            'totalCrimes' => $totalCrimes,
            'totalMurders' => $totalMurders,
            'totalAssault' => $totalAssault,
            'totalBurgalary' => $totalBurglary,
            'totalTheft' => $totalTheft,   
        );

        // Log::debug($totalCrimes);
        // Log::debug($totalMurders);
        // Log::debug($totalAssault);
        // Log::debug($totalTheif);
        // Log::debug($totalBurglary);
        return $result;

    }
       
}

