<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;


class SuburbDetailsController extends Controller
{
    public function index(Request $request)
    {
        #API key for AUSPOST, Move to secure location. 
        $ausPostAPIKey = "266251a0-f9c5-453b-a341-5fb4bf921c64";

        #Init array to collect all location details for return. 
        $collection = array();

        #Get entered postcode from API
        $postcode = $request['postcode'];
        
        #Init request to AUSPOST API
        $client = new Client();
        
        try {
            $res = $client->request('GET', "https://digitalapi.auspost.com.au/postcode/search.json?q=$postcode", [
                'headers' => [
                    'auth-key' =>  $ausPostAPIKey
                ]
            ]);
        } catch (Exception $e) {
            // $error = $e->session_status
            // $bob = $res->getStatusCode();
            $error = $e->getMessage();
            $message = "Please include location in request. \r\nRetuned: $error";
            return response($error,403)->header('Content-Type', 'text/plain');
        }

        #If request succeeds. 
        if ($res->getStatusCode() == 200) {

            #transform received data to JSON. 
            $received_data = $res->getBody();
            $received_json = json_decode($received_data, true);
            // Log::debug($res->getBody());

            #Depending on destination entered, the returned data can be one or two levels deep, resulting in errors.
            #this block rebuilds the array to be only 1 level deep. 
            try {
                $locality = $received_json['localities']['locality'];
                $locations = array();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $message = "Check location: doesnt exists. \r\nRetuned: $error";
                return $message;
            }
            if (!(array_key_exists("location", $locality))) {
                foreach ($locality as $location) {
                    array_push($locations, $location);
                }
            } else {
                array_push($locations, $locality);
            }


            // Log::debug($locations);
            // Log::debug(count($locations));
            #Extact only the required information from received data and add it to collection array for return. 
            try {
                foreach ($locations as $location) {
                    if(!(array_key_exists('latitude',$location)) || !(array_key_exists('longitude',$location))){
                        continue;
                    }
                    // Log::debug(array_key_exists('location',$location));
                    $thelocation = array(
                        "location" => $location['location'],
                        "postcode" => $location['postcode'],
                        "state" => $location['state'],
                        "latitude" => $location['latitude'],
                        "longitude" => $location['longitude']
                        
                    );
                    
                    array_push($collection, $thelocation);
                }
            } catch (Exception $e) {
                $error = $e->getMessage();
                $message = "Couldn't generate required data for the entered location. Retuned: $error";
                return $message;
            }
        }
        // Log::debug($collection);
        #return collection to caller. 
        return response($collection,200)->header('Content-Type', 'application/json');
    }
}
