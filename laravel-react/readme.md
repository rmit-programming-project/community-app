### Dependencies

- php
- composer
- laravel installer
- nodejs
- docker (not required yet)
- docker-compose (not required yet)

### How to run

first install all dependencies, cd /laravel-react-test

```
npm install
```

Now compile the project, from root of /laravel-react-test, run:

```
npm run dev
```

To serve the project, run:

```
php artisan serve
```

### Troubleshooting

If you run `php artisan serve` and get a `PHP Warning require(......)` messeage. run:

```
composer install
```

If you run the serve with artisan and receive an error 500 run: 

```
cp .env.example .env

php artisan key:generate
```
